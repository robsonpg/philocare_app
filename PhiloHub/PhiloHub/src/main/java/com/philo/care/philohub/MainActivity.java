package com.philo.care.philohub;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sxr.sdk.ble.keepfit.aidl.IRemoteService;
import com.sxr.sdk.ble.keepfit.aidl.IServiceCallback;
import com.sxr.sdk.ble.keepfit.client.R;
import com.sxr.sdk.ble.keepfit.client.SampleBleService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/*
Docs:

1.) Start service using alarmManager.
2.) Check in onResume if service is running & restart if not.
3.) Return START_STICKY from onStartCommand().
4.) In OnStartCommand() , create a thread and do the needful from that thread .All the logical stuff should be there in while(true).

The solutions in android 5 and higher is using AlarmManger and Broadcast Receiver

This is what I have used, for starting service after 30 seconds from current time,

Intent intent = new Intent(DashboardScreen.this, ServiceClass.class);
PendingIntent pintent = PendingIntent.getService(DashboardScreen.this, 0, intent, 0);
AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 30*1000, pintent);

 <service android:enabled="true" android:name=".ServiceClass" />

 */

public class MainActivity extends AppCompatActivity {

    // variáveis globais
    private String _device_code;

    private static final String TAG = MainActivity.class.getSimpleName();
    private IRemoteService mService;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

    private TextView data_text;
    private TextView tvSync;
    private boolean bColor = false;
    private ScrollView svLog;

    // botões da interface
    private Button btnSearch;

    // texto da interface
    private TextView txtDevice;

    // lista de dispositivos encontrados
    private ArrayList<BleDeviceItem> nearbyItemList;
    // flag de inicio do escanemento
    private boolean bStart = false;

    // contador de sono
    private int sleepcount = 0;

    // callback do mService
    protected String curMac;

    // arquivo e flag de log
    private String pathLog = "/philocare/log/";
    //private String pathLog = "/jyClient/log/";
    private boolean bSave = true;

    // flag de ligação com dispositivo bluetooth
    private boolean mIsBound = false;

    /*
    Broadcast receiver para o bluetooth
     */
    /*private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        // Bluetooth has been turned off;
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        // Bluetooth is turning off;
                        break;
                    case BluetoothAdapter.STATE_ON:
                        // Bluetooth is on
                        if (btManager == null) {
                            btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                        }
                        if (btAdapter == null) {
                            btAdapter = btManager.getAdapter();

                            // se o bluetooth ligar, fazer o bind
                            //bindBluetooth();
                        }
                        if (btScanner == null) {
                            btScanner = btAdapter.getBluetoothLeScanner();
                        }
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        // Bluetooth is turning on
                        break;
                }
            }
        }
    };*/


    /*
    Callback para retornar qual pulseira está conectada
     */
    private String callRemoteGetConnectedDevice() {
        String deviceMac = "";
        if (mService != null) {
            try {
                deviceMac = mService.getConnectedDevice();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(this, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }

        return deviceMac;
    }

    /*
    Callback para verificar se a pulseira está autorizada
     */
    private int callRemoteIsAuthorize() {
        int isAuthrize = 0;
        if (mService != null) {
            try {
                isAuthrize = mService.isAuthrize();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(this, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }

        return isAuthrize;
    }

    /*
    Callback que verifica se o dispositivo está conectado
     */
    private boolean callRemoteIsConnected() {
        boolean isConnected = false;
        if (mService != null) {
            try {
                isConnected = mService.isConnectBt();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(this, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }

        return isConnected;
    }

    /*
    Conecta com um dispositivo
     */
    private void callRemoteConnect(String name, String mac) {
        if (mac == null || mac.length() == 0) {
            Toast.makeText(this, "ble device mac address is not correctly!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mService != null) {
            try {
                mService.connectBt(name, mac);
                // desliga a tela de scan
                //dismissPopWindow();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(this, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    Callback para verificar qual estado de conecção está a pulseira
    state = 2 ok!
     */
    Handler updateConnectStateHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            //super.handleMessage(msg);
            Bundle data = msg.getData();
            int state = data.getInt("state");

            if (state == 2) {
                //llConnect.setVisibility(View.VISIBLE);
            } else {
                //llConnect.setVisibility(View.GONE);
            }
            return true;
        }
    });

    protected void updateConnectState(int state) {
        Message msg = new Message();
        Bundle data = new Bundle();
        data.putInt("state", state);
        msg.setData(data);
        updateConnectStateHandler.sendMessage(msg);
        // Fazer botões desaparecerem

    }


    private Handler messageHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Bundle data = msg.getData();
            String title = data.getString("title");
            String content = data.getString("content");

            Log.i(TAG, title + ": " + content);
            switch (title) {
                default:
                    String text = "[" + sdf.format(new Date()) + "] " + title + "\n" + content;
                    data_text.setText(text);
                    text = "<font color='" + (bColor ? "#00" : "#82") + "'>" + text.replace("\n", "<br>") + "</font><br>";
                    tvSync.append(Html.fromHtml(text));
                    //svLog.fullScroll(View.FOCUS_DOWN);
                    bColor = !bColor;
                    break;
            }
            return true;
        }
    });

    /*
    Exibe mensagens na tela
     */
    protected void showToast(String title, String content) {
        //String file = "demo.log";
        //if(bSave)
        //    SysUtils.writeTxtToFile(title + " -> " + content, pathLog, file);
        Message msg = new Message();
        Bundle data = new Bundle();
        data.putString("title", title);
        data.putString("content", content);
        msg.setData(data);
        messageHandler.sendMessage(msg);
    }

    private IServiceCallback mServiceCallback = new IServiceCallback.Stub() {
        @Override
        public void onConnectStateChanged(int state) throws RemoteException {
            showToast("onConnectStateChanged", curMac + " state " + state);
            updateConnectState(state);
        }


        @Override
        public void onScanCallback(String deviceName, String deviceMacAddress, int rssi)
                throws RemoteException {
            Log.i(TAG, String.format("onScanCallback <%1$s>[%2$s](%3$d)", deviceName, deviceMacAddress, rssi));

            /*if(nearbyItemList == null)
                return;

            // Modificação para encontrar sempre a pulseira cadastrada
            // Atual: "A4:F6:3D:B8:FB:8C"

            Iterator<BleDeviceItem> iter = nearbyItemList.iterator();
            BleDeviceItem item = null;
            boolean bExist = false;
            while (iter.hasNext()) {

                item = (BleDeviceItem) iter.next();
                if (item.getBleDeviceAddress().equalsIgnoreCase(deviceMacAddress) == true) {
                    bExist = true;
                    // mostra a proximidade da pulseira
                    item.setRssi(rssi);
                    if (rssi > -90) {
                        // desliga a tela de scan
                        //dismissPopWindow();
                        // conecta a pulseira
                        //callRemoteConnect(deviceName, deviceMacAddress);
                        //mService.connectBt(deviceName, deviceMacAddress);
                    }
                    break;
                }
            }
            // Aqui começa a alteração para detectar apenas a pulseira de interesse
            if (!bExist) {
                item = new BleDeviceItem(deviceName, deviceMacAddress, "", "", rssi, "");
                nearbyItemList.add(item);
                // se quisermos ordenar a list, criar a função abaixo
                //Collections.sort(nearbyItemList, new ComparatorBleDeviceItem());
            }

            Message msg = new Message();
            scanDeviceHandler.sendMessage(msg);*/

        }


        @Override
        public void onSetNotify(int result) throws RemoteException {
            showToast("onSetNotify", String.valueOf(result));
        }

        @Override
        public void onSetUserInfo(int result) throws RemoteException {
            showToast("onSetUserInfo", "" + result);
        }

        @Override
        public void onAuthSdkResult(int errorCode) throws RemoteException {
            showToast("onAuthSdkResult", errorCode + "");
            /*if (errorCode == 200) {
                // habilita o scan se conectou no serviço de bluetooth
                callRemoteScanDevice();
            }*/
            btnSearch.setEnabled(true);
        }

        @Override
        public void onGetDeviceTime(int result, String time) throws RemoteException {
            showToast("onGetDeviceTime", String.valueOf(time));
        }

        @Override
        public void onSetDeviceTime(int arg0) throws RemoteException {
            showToast("onSetDeviceTime", arg0 + "");
        }

        @Override
        public void onSetDeviceInfo(int arg0) throws RemoteException {
            showToast("onSetDeviceInfo", arg0 + "");
        }


        @Override
        public void onAuthDeviceResult(int arg0) throws RemoteException {
            showToast("onAuthDeviceResult", arg0 + "");
        }


        @Override
        public void onSetAlarm(int arg0) throws RemoteException {
            showToast("onSetAlarm", arg0 + "");
        }

        @Override
        public void onSendVibrationSignal(int arg0) throws RemoteException {
            showToast("onSendVibrationSignal", "result:" + arg0);
        }

        @Override
        public void onGetDeviceBatery(int arg0, int arg1)
                throws RemoteException {
            showToast("onGetDeviceBatery", "batery:" + arg0 + ", statu " + arg1);
        }


        @Override
        public void onSetDeviceMode(int arg0) throws RemoteException {
            showToast("onSetDeviceMode", "result:" + arg0);
        }

        @Override
        public void onSetHourFormat(int arg0) throws RemoteException {
            showToast("onSetHourFormat ", "result:" + arg0);

        }

        @Override
        public void setAutoHeartMode(int arg0) throws RemoteException {
            showToast("setAutoHeartMode ", "result:" + arg0);
        }


        @Override
        public void onGetCurSportData(int type, long timestamp, int step, int distance,
                                      int cal, int cursleeptime, int totalrunningtime, int steptime) throws RemoteException {
            Date date = new Date(timestamp * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String time = sdf.format(date);
            showToast("onGetCurSportData", "type : " + type + " , time :" + time + " , step: " + step + ", distance :" + distance + ", cal :" + cal + ", cursleeptime :" + cursleeptime + ", totalrunningtime:" + totalrunningtime);


        }

        @Override
        public void onGetSenserData(int result, long timestamp, int heartrate, int sleepstatu)
                throws RemoteException {
            Date date = new Date(timestamp * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String time = sdf.format(date);
            showToast("onGetSenserData", "result: " + result + ",time:" + time + ",heartrate:" + heartrate + ",sleepstatu:" + sleepstatu);

        }


        @Override
        public void onGetDataByDay(int type, long timestamp, int step, int heartrate)
                throws RemoteException {
            Date date = new Date(timestamp * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String recorddate = sdf.format(date);
            showToast("onGetDataByDay", "type:" + type + ",time::" + recorddate + ",step:" + step + ",heartrate:" + heartrate);
            if (type == 2) {
                sleepcount++;
            }
        }

        @Override
        public void onGetDataByDayEnd(int type, long timestamp) throws RemoteException {
            Date date = new Date(timestamp * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String recorddate = sdf.format(date);
            showToast("onGetDataByDayEnd", "time:" + recorddate + ",sleepcount:" + sleepcount);
            sleepcount = 0;
        }


        @Override
        public void onSetPhontMode(int arg0) throws RemoteException {
            showToast("onSetPhontMode", "result:" + arg0);
        }


        @Override
        public void onSetSleepTime(int arg0) throws RemoteException {
            showToast("onSetSleepTime", "result:" + arg0);
        }


        @Override
        public void onSetIdleTime(int arg0) throws RemoteException {
            showToast("onSetIdleTime", "result:" + arg0);
        }


        @Override
        public void onGetDeviceInfo(int version, String macaddress, String vendorCode,
                                    String productCode, int result) throws RemoteException {
            showToast("onGetDeviceInfo", "version :" + version + ",macaddress : " + macaddress + ",vendorCode : " + vendorCode + ",productCode :" + productCode + " , CRCresult :" + result);

        }

        @Override
        public void onGetDeviceAction(int type) throws RemoteException {
            showToast("onGetDeviceAction", "type:" + type);
        }


        @Override
        public void onGetBandFunction(int result, boolean[] results) throws RemoteException {
            showToast("onGetBandFunction", "result : " + result + ", results :" + results.length);

            String function = "";
            for(int i = 0; i < results.length; i ++){
                function += String.valueOf((i+1) + "=" + results[i] + " ");
            }
            showToast("onGetBandFunction", function);
        }

        @Override
        public void onSetLanguage(int arg0) throws RemoteException {
            showToast("onSetLanguage", "result:" + arg0);
        }


        @Override
        public void onSendWeather(int arg0) throws RemoteException {
            showToast("onSendWeather", "result:" + arg0);
        }


        @Override
        public void onSetAntiLost(int arg0) throws RemoteException {
            showToast("onSetAntiLost", "result:" + arg0);

        }


        @Override
        public void onReceiveSensorData(int arg0, int arg1, int arg2, int arg3,
                                        int arg4) throws RemoteException {
            showToast("onReceiveSensorData", "result:" + arg0 + " , " + arg1 + " , " + arg2 + " , " + arg3 + " , " + arg4);
        }


        @Override
        public void onSetBloodPressureMode(int arg0) throws RemoteException {
            showToast("onSetBloodPressureMode", "result:" + arg0);
        }


        @Override
        public void onGetMultipleSportData(int flag, String recorddate, int mode, int value)
                throws RemoteException {
//            Date date = new Date(timestamp * 1000);
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            String recorddate = sdf.format(date);
            showToast("onGetMultipleSportData", "flag:" + flag + " , mode :" + mode + " recorddate:" + recorddate + " , value :" + value);
        }


        @Override
        public void onSetGoalStep(int result) throws RemoteException {
            showToast("onSetGoalStep", "result:" + result);
        }


        @Override
        public void onSetDeviceHeartRateArea(int result) throws RemoteException {
            showToast("onSetDeviceHeartRateArea", "result:" + result);
        }


        @Override
        public void onSensorStateChange(int type, int state)
                throws RemoteException {

            showToast("onSensorStateChange", "type:" + type + " , state : " + state);
        }

        @Override
        public void onReadCurrentSportData(int mode, String time, int step,
                                           int cal) throws RemoteException {

            showToast("onReadCurrentSportData", "mode:" + mode + " , time : " + time + " , step : " + step + " cal :" + cal);
        }

        @Override
        public void onGetOtaInfo(boolean isUpdate, String version, String path) throws RemoteException {
            showToast("onGetOtaInfo", "isUpdate " + isUpdate + " version " + version + " path " + path);
        }

        @Override
        public void onGetOtaUpdate(int step, int progress) throws RemoteException {
            showToast("onGetOtaUpdate", "step " + step + " progress " + progress);
        }

        @Override
        public void onSetDeviceCode(int result) throws RemoteException {
            showToast("onSetDeviceCode", "result " + result);
        }

        @Override
        public void onGetDeviceCode(byte[] bytes) throws RemoteException {
            //showToast("onGetDeviceCode", "bytes " + SysUtils.printHexString(bytes));
        }

        @Override
        public void onCharacteristicChanged(String uuid, byte[] bytes) throws RemoteException {
            //showToast("onCharacteristicChanged", uuid + " " + SysUtils.printHexString(bytes));
        }

        @Override
        public void onCharacteristicWrite(String uuid, byte[] bytes, int status) throws RemoteException {
            //showToast("onCharacteristicWrite", status + " " + uuid + " " + SysUtils.printHexString(bytes));
        }
    };


    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Toast.makeText(MainActivity.this, "Service connected", Toast.LENGTH_SHORT).show();

            mService = IRemoteService.Stub.asInterface(service);
            try {
                mService.registerCallback(mServiceCallback);
                mService.openSDKLog(bSave, pathLog, "blue.log");

                // verifica se já tem algum dispositivo conectado
                boolean isConnected = callRemoteIsConnected();

                // se nã tem, desabilita as funções de requisição
                if (isConnected == false) {
                    // disconectou
                    //llConnect.setVisibility(View.GONE);
                } else {
                    // pulseira autorizada
                    int authrize = callRemoteIsAuthorize();
                    if (authrize == 200) {
                        // se está autorizado, conecta na pulseira
                        String curMac = callRemoteGetConnectedDevice();
                        //llConnect.setVisibility(View.VISIBLE);
                    }
                }

            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(MainActivity.this, "Service disconnected", Toast.LENGTH_SHORT).show();
            //llConnect.setVisibility(View.GONE);
            mService = null;
        }
    };

    // variáveis para o detector de Bluetooth (dispositivos)
    /*private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private BluetoothLeScanner btScanner;
    private final static int REQUEST_ENABLE_BT = 1;*/
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSearch = (Button) findViewById(R.id.btnSearch);

        tvSync = (TextView) findViewById(R.id.tvConnection);
        data_text = (TextView) findViewById(R.id.tvData);

        // Register for broadcasts on BluetoothAdapter state change
        /*IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);

        btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
        btScanner = btAdapter.getBluetoothLeScanner();


        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
        }*/

        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.app_need_loc);
            builder.setMessage(R.string.ask_grant_loc);
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                }
            });
            builder.show();
        }

        //deviceTextHandler();

        /*Intent gattServiceIntent = new Intent(this,
                SampleBleService.class);
        gattServiceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startService(gattServiceIntent);*/

        // executa o serviço de BLE para fazer bind
        Intent gattServiceIntent = new Intent(this,
                SampleBleService.class);
        gattServiceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(gattServiceIntent);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(gattServiceIntent);
        } else {
            this.startService(gattServiceIntent);
        }*/

        // faz bind com o serviço
        /*Intent service_intent = new Intent(IRemoteService.class.getName());
        service_intent.setClassName("com.sxr.sdk.ble.keepfit.client",
                "com.sxr.sdk.ble.keepfit.client.SampleBleService");
        bindService(service_intent, mServiceConnection, BIND_AUTO_CREATE);*/

        // faz bind com o serviço
        Intent service_intent = new Intent(IRemoteService.class.getName());
        service_intent.setClassName("com.sxr.sdk.ble.keepfit.client",
                "com.sxr.sdk.ble.keepfit.client.SampleBleService");
        //service_intent.setClassName("com.philo.care.philohub", "com.philo.care.philohub.BleService");
        bindService(service_intent, mServiceConnection, BIND_AUTO_CREATE);
        mIsBound = true;

        //startService(gattServiceIntent);

        /*Intent intent = new Intent(MainActivity.this, BleService.class);
        PendingIntent pintent = PendingIntent.getService(MainActivity.this, 0, intent, 0);
        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pintent);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 30*1000, pintent);*/

        //bindBluetooth();
    }

    /*
    onResume da aplicação
    Caso a aplicação retorne, temos de verificar o bluetooth
     */
    @Override
    protected void onResume() {
        super.onResume();

        //if (btAdapter != null && !btAdapter.isEnabled()) {
        //    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        //    startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
        //}
    }

    /*
    onStop da aplicação
     */
    @Override
    public void onStop() {
        super.onStop();

        // ...

        // Unregister broadcast listeners
        //unregisterReceiver(mReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregister broadcast listeners
        //unregisterReceiver(mReceiver);
    }

    //#############################################################################################
    // AÇÕES DE BOTÕES
    //#############################################################################################
    /** Called when the user taps the search button */
    public void bindBluetooth(View view) {
        // create service to search
        //Intent gattServiceIntent = new Intent(this,
        //        BleService.class);
        //gattServiceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //startService(gattServiceIntent);

        // faz bind com o serviço
        Intent service_intent = new Intent(IRemoteService.class.getName());
        service_intent.setClassName("com.sxr.sdk.ble.keepfit.client",
                "com.sxr.sdk.ble.keepfit.client.SampleBleService");
        //service_intent.setClassName("com.philo.care.philohub", "com.philo.care.philohub.BleService");
        bindService(service_intent, mServiceConnection, BIND_AUTO_CREATE);
        mIsBound = true;
    }

    /*
    Ativa a tela de configuração
     */
    public void btnSettingsClick(View view) {
        Intent intent = new Intent(
                MainActivity.this, SettingsActivity.class
        );
        startActivity(intent);
    }
}