package com.philo.care.philohub;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;

import com.sxr.sdk.ble.keepfit.client.R;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {

    // arquivo e flag de log
    private String pathConfig = "/philocare/";
    private String pathLog = "/philocare/log/";
    private String configFileName = "config.cnf";


    // adapter para adicionar itens no listview
    ArrayAdapter<String> listAdapter;

    // itens do list view
    ArrayList<String> listItems=new ArrayList<String>();

    // lista de dispositivos
    ListView deviceListView;

    // lista de dispositivos encontrados
    private ArrayList<BleDeviceItem> nearbyItemList;

    // adaptador para a lista de dispositivos
    private listDeviceViewAdapter nearbyListAdapter;

    // janela que mostra a lista de itens encontrados
    private PopupWindow window;

    // callback do mService
    protected String curMac;

    int clickCounter=0;

    /*
    Lista de propriedades que um item na lista de visualizações de dispositivos
     */
    private class ViewHolder {
        TextView tvName;
        TextView address;
        TextView rssi;
        String name;
        String mac;
    }

    /*
    Adaptador para exibição dos itens da lista de dispositivos encontrados
     */
    class listDeviceViewAdapter extends BaseAdapter implements
            AdapterView.OnItemSelectedListener {

        private static final int DEVICE_NEARBY = 0;
        int count = 0;
        private LayoutInflater layoutInflater;
        Context local_context;
        float xDown = 0, yDown = 0, xUp = 0, yUp = 0;
        private List<BleDeviceItem> itemList;
        private int type;
        protected AnimationDrawable adCallBand;

        public listDeviceViewAdapter(Context context, List<BleDeviceItem> list) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //layoutInflater = LayoutInflater.from(context);
            local_context = context;
            itemList = list;
        }

        public int getCount() {
            return itemList.size();
        }

        public Object getItem(int pos) {
            return pos;
        }

        public long getItemId(int pos) {
            return pos;
        }

        public View getView(int pos, View v, ViewGroup p) {
            View view;
            SettingsActivity.ViewHolder viewHolder;

            BleDeviceItem item = itemList.get(pos);

            view = layoutInflater.inflate(R.layout.device_listitem_text, null);
            viewHolder = new SettingsActivity.ViewHolder();

            view.setTag(viewHolder);
            viewHolder.tvName = (TextView) view.findViewById(R.id.ItemTitle);
            viewHolder.address = (TextView) view.findViewById(R.id.ItemDate);
            viewHolder.rssi = (TextView) view.findViewById(R.id.ItemRssi);

            viewHolder.tvName.setText(item.getBleDeviceName());
            viewHolder.address.setText(item.getBleDeviceAddress());
            int rssi = item.getRssi();
            viewHolder.rssi.setText(String.valueOf(rssi));
            viewHolder.name = item.getBleDeviceName();
            viewHolder.mac = item.getBleDeviceAddress();

            return view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position,
                                   long id) {
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

    }

    // variáveis para o detector de Bluetooth (dispositivos)
    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private BluetoothLeScanner btScanner;
    private final static int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        /*getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();*/
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        deviceListView = (ListView) findViewById(R.id.lvRegDevices);

        listAdapter = new ArrayAdapter<String>(this,
               android.R.layout.simple_list_item_1,
                listItems);
        deviceListView.setAdapter(listAdapter);

        btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
        btScanner = btAdapter.getBluetoothLeScanner();

        deviceTextHandler();
    }

    /*
    Manipula a inserção
     */
    public void addItems(View v) {
        listItems.add("Clicked : "+clickCounter++);
        listAdapter.notifyDataSetChanged();
    }

    /*
    Manipulador do dispositivo salvo para exibição na tela
     */
    private void deviceTextHandler() {
        // local para salvar o código da pulseira selecionada
        String device_code = getSavedDevice();

        if (!device_code.equals("")) {
            String[] names = device_code.split(";");
            listAdapter.add(getString(R.string.device) + names[0] + getString(R.string.new_line) +
                    getString(R.string.code) + names[1]);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    Recupera o nome e código do dispositivo salvo no arquivo de preferências
    */
    private String getSavedDevice () {

        String ret = "";
        String name_code = "";
        char[] buf = new char[256];
        Context context = this;

        try{
            File file = new File(getFilesDir() + pathConfig);
            File gpxfile = new File(file, configFileName);
            FileReader reader = new FileReader(gpxfile);
            reader.read(buf);
            reader.close();

            name_code = new String(buf);

        }catch (Exception e){
            e.printStackTrace();

        }

        return name_code;
    }


    /*
    Exibe lista de dispositivos que estão sendo encontrados no scan
     */
    public void popWindow(View parent, int windowRes) {
        if (window == null) {
            LayoutInflater lay = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popView = lay.inflate(windowRes, null);

            nearbyItemList = new ArrayList<BleDeviceItem>();

            ListView nearbyListView = (ListView) popView.findViewById(R.id.nearby_device_listView);

            nearbyListAdapter = new listDeviceViewAdapter(this, nearbyItemList);
            nearbyListAdapter.setType(listDeviceViewAdapter.DEVICE_NEARBY);
            nearbyListView.setAdapter(nearbyListAdapter);

            nearbyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        SettingsActivity.ViewHolder holder = (SettingsActivity.ViewHolder) view.getTag();
                        //callRemoteScanDevice();
                        //callRemoteDisconnect();
                        curMac = holder.mac;
                        // salvar a pulseira selecionada em arquivo para o serviço de atualização
                        // usar
                        // local para salvar o código da pulseira selecionada
                        saveDevice(holder.name, holder.mac);
                        deviceTextHandler();
                        dismissPopWindow();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("BLE service", "ble connect ble device: excption");
                    }
                }
            });

            popView.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
                    window.dismiss();
                    window = null;
                    return false;
                }

            });

            window = new PopupWindow(popView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
            window.setOutsideTouchable(true);
            window.setFocusable(true);
            window.update();
            window.showAtLocation(parent, Gravity.CENTER_VERTICAL, 0, 0);
            window.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    //bStart = true;
                    //callRemoteScanDevice();
                    window = null;
                }
            });
        }
    }

    /*
    Esconde a tela de detecção dos dispositivos
     */
    public boolean dismissPopWindow() {
        if (window != null) {
            window.dismiss();
            window = null;

            return true;
        }

        return false;
    }

    /*
    Busca os dispositivos BLE no alcance
     */
    public void searchDevice(View view) {
        // habilita o scan se conectou no serviço de bluetooth
        //callRemoteScanDevice();
        System.out.println("start scanning");
        popWindow(findViewById(R.id.btnSearch), R.layout.popwindow_devicelist);
        //btnSearch.setVisibility(View.INVISIBLE);
        //stopScanningButton.setVisibility(View.VISIBLE);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                btScanner.startScan(leScanCallback);
            }
        });
    }

    /*
    Salva o nome e código da pulseira no arquivo de preferências
     */
    private void saveDevice (String name, String code) throws IOException {

        Context context = this;

        try{
            File file = new File(getFilesDir() + pathConfig);
            if(!file.exists()){
                file.mkdir();
            }
            // por enquanto vamos fazer apenas de um dispositivo
            File gpxfile = new File(file, configFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.write(name + ';' + code);
            writer.flush();
            writer.close();

        }catch (Exception e){
            e.printStackTrace();

        }
    }

    /*
    Callback do scaneamento bluetooth
     */
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {

            if(nearbyItemList == null)
                return;

            // Modificação para encontrar sempre a pulseira cadastrada
            // Atual: "A4:F6:3D:B8:FB:8C"

            Iterator<BleDeviceItem> iter = nearbyItemList.iterator();
            BleDeviceItem item = null;
            boolean bExist = false;
            while (iter.hasNext()) {

                item = (BleDeviceItem) iter.next();
                if (item.getBleDeviceAddress().equalsIgnoreCase(result.getDevice().getAddress()) == true) {
                    bExist = true;
                    // mostra a proximidade da pulseira
                    item.setRssi(result.getRssi());
                    if (result.getRssi() > -90) {
                        // desliga a tela de scan
                        //dismissPopWindow();
                        // conecta a pulseira
                        //callRemoteConnect(deviceName, deviceMacAddress);
                        //mService.connectBt(deviceName, deviceMacAddress);
                    }
                    break;
                }
            }
            // Aqui começa a alteração para detectar apenas a pulseira de interesse
            if (!bExist) {
                item = new BleDeviceItem(result.getDevice().getName(), result.getDevice().getAddress(),
                        "", "", result.getRssi(), "");
                nearbyItemList.add(item);
                // se quisermos ordenar a list, criar a função abaixo
                //Collections.sort(nearbyItemList, new ComparatorBleDeviceItem());
            }

            Message msg = new Message();
            scanDeviceHandler.sendMessage(msg);


            //peripheralTextView.append("Device Name: " + result.getDevice().getName() + " rssi: " + result.getRssi() + "\n");

            // auto scroll for text view
            //final int scrollAmount = peripheralTextView.getLayout().getLineTop(peripheralTextView.getLineCount()) - peripheralTextView.getHeight();
            // if there is no need to scroll, scrollAmount will be <=0
            //if (scrollAmount > 0)
            //    peripheralTextView.scrollTo(0, scrollAmount);
        }
    };

    /*
    Manipulador para mensagens da lista de detecção
     */
    Handler scanDeviceHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            //super.handleMessage(msg);
            Bundle data = msg.getData();
            String result = data.getString("result");
            nearbyListAdapter.notifyDataSetChanged();
            return true;
        }
    });

}