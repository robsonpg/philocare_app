// Generated code from Butter Knife. Do not modify!
package com.philocare.philohub.activity;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import com.philocare.philohub.R;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class PttActivity_ViewBinding<T extends PttActivity> implements Unbinder {
  protected T target;

  private View view2131230864;

  private View view2131230863;

  public PttActivity_ViewBinding(final T target, Finder finder, Object source) {
    this.target = target;

    View view;
    target.mPttModelTv = finder.findRequiredViewAsType(source, R.id.ptt_model, "field 'mPttModelTv'", TextView.class);
    target.ecgHeartRealthView = finder.findRequiredViewAsType(source, R.id.ptt_real_view, "field 'ecgHeartRealthView'", EcgHeartRealthView.class);
    view = finder.findRequiredView(source, R.id.ptt_sign_open, "method 'enter'");
    view2131230864 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.enter();
      }
    });
    view = finder.findRequiredView(source, R.id.ptt_sign_close, "method 'exitModel'");
    view2131230863 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.exitModel();
      }
    });
  }

  @Override
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mPttModelTv = null;
    target.ecgHeartRealthView = null;

    view2131230864.setOnClickListener(null);
    view2131230864 = null;
    view2131230863.setOnClickListener(null);
    view2131230863 = null;

    this.target = null;
  }
}
