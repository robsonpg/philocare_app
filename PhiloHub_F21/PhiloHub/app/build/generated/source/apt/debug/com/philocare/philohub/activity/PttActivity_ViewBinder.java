// Generated code from Butter Knife. Do not modify!
package com.philocare.philohub.activity;

import butterknife.Unbinder;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.Object;
import java.lang.Override;

public final class PttActivity_ViewBinder implements ViewBinder<PttActivity> {
  @Override
  public Unbinder bind(Finder finder, PttActivity target, Object source) {
    return new PttActivity_ViewBinding<>(target, finder, source);
  }
}
