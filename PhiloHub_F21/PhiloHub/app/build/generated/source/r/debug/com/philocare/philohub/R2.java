// Generated code from Butter Knife gradle plugin. Do not modify!
package com.philocare.philohub;

import android.support.annotation.AttrRes;
import android.support.annotation.BoolRes;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;

public final class R2 {
  public static final class attr {
    @AttrRes
    public static final int actionBarDivider = 0x7f030000;

    @AttrRes
    public static final int actionBarItemBackground = 0x7f030001;

    @AttrRes
    public static final int actionBarPopupTheme = 0x7f030002;

    @AttrRes
    public static final int actionBarSize = 0x7f030003;

    @AttrRes
    public static final int actionBarSplitStyle = 0x7f030004;

    @AttrRes
    public static final int actionBarStyle = 0x7f030005;

    @AttrRes
    public static final int actionBarTabBarStyle = 0x7f030006;

    @AttrRes
    public static final int actionBarTabStyle = 0x7f030007;

    @AttrRes
    public static final int actionBarTabTextStyle = 0x7f030008;

    @AttrRes
    public static final int actionBarTheme = 0x7f030009;

    @AttrRes
    public static final int actionBarWidgetTheme = 0x7f03000a;

    @AttrRes
    public static final int actionButtonStyle = 0x7f03000b;

    @AttrRes
    public static final int actionDropDownStyle = 0x7f03000c;

    @AttrRes
    public static final int actionLayout = 0x7f03000d;

    @AttrRes
    public static final int actionMenuTextAppearance = 0x7f03000e;

    @AttrRes
    public static final int actionMenuTextColor = 0x7f03000f;

    @AttrRes
    public static final int actionModeBackground = 0x7f030010;

    @AttrRes
    public static final int actionModeCloseButtonStyle = 0x7f030011;

    @AttrRes
    public static final int actionModeCloseDrawable = 0x7f030012;

    @AttrRes
    public static final int actionModeCopyDrawable = 0x7f030013;

    @AttrRes
    public static final int actionModeCutDrawable = 0x7f030014;

    @AttrRes
    public static final int actionModeFindDrawable = 0x7f030015;

    @AttrRes
    public static final int actionModePasteDrawable = 0x7f030016;

    @AttrRes
    public static final int actionModePopupWindowStyle = 0x7f030017;

    @AttrRes
    public static final int actionModeSelectAllDrawable = 0x7f030018;

    @AttrRes
    public static final int actionModeShareDrawable = 0x7f030019;

    @AttrRes
    public static final int actionModeSplitBackground = 0x7f03001a;

    @AttrRes
    public static final int actionModeStyle = 0x7f03001b;

    @AttrRes
    public static final int actionModeWebSearchDrawable = 0x7f03001c;

    @AttrRes
    public static final int actionOverflowButtonStyle = 0x7f03001d;

    @AttrRes
    public static final int actionOverflowMenuStyle = 0x7f03001e;

    @AttrRes
    public static final int actionProviderClass = 0x7f03001f;

    @AttrRes
    public static final int actionViewClass = 0x7f030020;

    @AttrRes
    public static final int activityChooserViewStyle = 0x7f030021;

    @AttrRes
    public static final int additionalPaddingLeftForIcon = 0x7f030022;

    @AttrRes
    public static final int additionalPaddingRightForIcon = 0x7f030023;

    @AttrRes
    public static final int alertDialogButtonGroupStyle = 0x7f030024;

    @AttrRes
    public static final int alertDialogCenterButtons = 0x7f030025;

    @AttrRes
    public static final int alertDialogStyle = 0x7f030026;

    @AttrRes
    public static final int alertDialogTheme = 0x7f030027;

    @AttrRes
    public static final int allowStacking = 0x7f030028;

    @AttrRes
    public static final int alpha = 0x7f030029;

    @AttrRes
    public static final int alphabeticModifiers = 0x7f03002a;

    @AttrRes
    public static final int arrowHeadLength = 0x7f03002b;

    @AttrRes
    public static final int arrowShaftLength = 0x7f03002c;

    @AttrRes
    public static final int autoCompleteTextViewStyle = 0x7f03002d;

    @AttrRes
    public static final int autoSizeMaxTextSize = 0x7f03002e;

    @AttrRes
    public static final int autoSizeMinTextSize = 0x7f03002f;

    @AttrRes
    public static final int autoSizePresetSizes = 0x7f030030;

    @AttrRes
    public static final int autoSizeStepGranularity = 0x7f030031;

    @AttrRes
    public static final int autoSizeTextType = 0x7f030032;

    @AttrRes
    public static final int background = 0x7f030033;

    @AttrRes
    public static final int backgroundSplit = 0x7f030034;

    @AttrRes
    public static final int backgroundStacked = 0x7f030035;

    @AttrRes
    public static final int backgroundTint = 0x7f030036;

    @AttrRes
    public static final int backgroundTintMode = 0x7f030037;

    @AttrRes
    public static final int barLength = 0x7f030038;

    @AttrRes
    public static final int behavior_autoHide = 0x7f030039;

    @AttrRes
    public static final int behavior_fitToContents = 0x7f03003a;

    @AttrRes
    public static final int behavior_hideable = 0x7f03003b;

    @AttrRes
    public static final int behavior_overlapTop = 0x7f03003c;

    @AttrRes
    public static final int behavior_peekHeight = 0x7f03003d;

    @AttrRes
    public static final int behavior_skipCollapsed = 0x7f03003e;

    @AttrRes
    public static final int borderWidth = 0x7f03003f;

    @AttrRes
    public static final int borderlessButtonStyle = 0x7f030040;

    @AttrRes
    public static final int bottomAppBarStyle = 0x7f030041;

    @AttrRes
    public static final int bottomNavigationStyle = 0x7f030042;

    @AttrRes
    public static final int bottomSheetDialogTheme = 0x7f030043;

    @AttrRes
    public static final int bottomSheetStyle = 0x7f030044;

    @AttrRes
    public static final int boxBackgroundColor = 0x7f030045;

    @AttrRes
    public static final int boxBackgroundMode = 0x7f030046;

    @AttrRes
    public static final int boxCollapsedPaddingBottom = 0x7f030047;

    @AttrRes
    public static final int boxCollapsedPaddingTop = 0x7f030048;

    @AttrRes
    public static final int boxCornerRadiusBottomLeft = 0x7f030049;

    @AttrRes
    public static final int boxCornerRadiusBottomRight = 0x7f03004a;

    @AttrRes
    public static final int boxCornerRadiusTopLeft = 0x7f03004b;

    @AttrRes
    public static final int boxCornerRadiusTopRight = 0x7f03004c;

    @AttrRes
    public static final int boxExpandedPaddingBottom = 0x7f03004d;

    @AttrRes
    public static final int boxExpandedPaddingTop = 0x7f03004e;

    @AttrRes
    public static final int boxPaddingLeft = 0x7f03004f;

    @AttrRes
    public static final int boxPaddingRight = 0x7f030050;

    @AttrRes
    public static final int boxStrokeColor = 0x7f030051;

    @AttrRes
    public static final int boxStrokeWidth = 0x7f030052;

    @AttrRes
    public static final int buttonBarButtonStyle = 0x7f030053;

    @AttrRes
    public static final int buttonBarNegativeButtonStyle = 0x7f030054;

    @AttrRes
    public static final int buttonBarNeutralButtonStyle = 0x7f030055;

    @AttrRes
    public static final int buttonBarPositiveButtonStyle = 0x7f030056;

    @AttrRes
    public static final int buttonBarStyle = 0x7f030057;

    @AttrRes
    public static final int buttonGravity = 0x7f030058;

    @AttrRes
    public static final int buttonIconDimen = 0x7f030059;

    @AttrRes
    public static final int buttonPanelSideLayout = 0x7f03005a;

    @AttrRes
    public static final int buttonStyle = 0x7f03005b;

    @AttrRes
    public static final int buttonStyleSmall = 0x7f03005c;

    @AttrRes
    public static final int buttonTint = 0x7f03005d;

    @AttrRes
    public static final int buttonTintMode = 0x7f03005e;

    @AttrRes
    public static final int cardBackgroundColor = 0x7f03005f;

    @AttrRes
    public static final int cardCornerRadius = 0x7f030060;

    @AttrRes
    public static final int cardElevation = 0x7f030061;

    @AttrRes
    public static final int cardMaxElevation = 0x7f030062;

    @AttrRes
    public static final int cardPreventCornerOverlap = 0x7f030063;

    @AttrRes
    public static final int cardUseCompatPadding = 0x7f030064;

    @AttrRes
    public static final int cardViewStyle = 0x7f030065;

    @AttrRes
    public static final int checkboxStyle = 0x7f030066;

    @AttrRes
    public static final int checkedChip = 0x7f030067;

    @AttrRes
    public static final int checkedIcon = 0x7f030068;

    @AttrRes
    public static final int checkedIconEnabled = 0x7f030069;

    @AttrRes
    public static final int checkedTextViewStyle = 0x7f03006a;

    @AttrRes
    public static final int chipBackgroundColor = 0x7f03006b;

    @AttrRes
    public static final int chipCornerRadius = 0x7f03006c;

    @AttrRes
    public static final int chipEndPadding = 0x7f03006d;

    @AttrRes
    public static final int chipGroupStyle = 0x7f03006e;

    @AttrRes
    public static final int chipIcon = 0x7f03006f;

    @AttrRes
    public static final int chipIconEnabled = 0x7f030070;

    @AttrRes
    public static final int chipIconSize = 0x7f030071;

    @AttrRes
    public static final int chipMinHeight = 0x7f030072;

    @AttrRes
    public static final int chipSpacing = 0x7f030073;

    @AttrRes
    public static final int chipSpacingHorizontal = 0x7f030074;

    @AttrRes
    public static final int chipSpacingVertical = 0x7f030075;

    @AttrRes
    public static final int chipStandaloneStyle = 0x7f030076;

    @AttrRes
    public static final int chipStartPadding = 0x7f030077;

    @AttrRes
    public static final int chipStrokeColor = 0x7f030078;

    @AttrRes
    public static final int chipStrokeWidth = 0x7f030079;

    @AttrRes
    public static final int chipStyle = 0x7f03007a;

    @AttrRes
    public static final int chipText = 0x7f03007b;

    @AttrRes
    public static final int closeIcon = 0x7f03007c;

    @AttrRes
    public static final int closeIconEnabled = 0x7f03007d;

    @AttrRes
    public static final int closeIconEndPadding = 0x7f03007e;

    @AttrRes
    public static final int closeIconSize = 0x7f03007f;

    @AttrRes
    public static final int closeIconStartPadding = 0x7f030080;

    @AttrRes
    public static final int closeIconTint = 0x7f030081;

    @AttrRes
    public static final int closeItemLayout = 0x7f030082;

    @AttrRes
    public static final int collapseContentDescription = 0x7f030083;

    @AttrRes
    public static final int collapseIcon = 0x7f030084;

    @AttrRes
    public static final int collapsedTitleGravity = 0x7f030085;

    @AttrRes
    public static final int collapsedTitleTextAppearance = 0x7f030086;

    @AttrRes
    public static final int color = 0x7f030087;

    @AttrRes
    public static final int colorAccent = 0x7f030088;

    @AttrRes
    public static final int colorBackgroundFloating = 0x7f030089;

    @AttrRes
    public static final int colorButtonNormal = 0x7f03008a;

    @AttrRes
    public static final int colorControlActivated = 0x7f03008b;

    @AttrRes
    public static final int colorControlHighlight = 0x7f03008c;

    @AttrRes
    public static final int colorControlNormal = 0x7f03008d;

    @AttrRes
    public static final int colorError = 0x7f03008e;

    @AttrRes
    public static final int colorPrimary = 0x7f03008f;

    @AttrRes
    public static final int colorPrimary100 = 0x7f030090;

    @AttrRes
    public static final int colorPrimary200 = 0x7f030091;

    @AttrRes
    public static final int colorPrimary300 = 0x7f030092;

    @AttrRes
    public static final int colorPrimary400 = 0x7f030093;

    @AttrRes
    public static final int colorPrimary50 = 0x7f030094;

    @AttrRes
    public static final int colorPrimary500 = 0x7f030095;

    @AttrRes
    public static final int colorPrimary600 = 0x7f030096;

    @AttrRes
    public static final int colorPrimary700 = 0x7f030097;

    @AttrRes
    public static final int colorPrimary800 = 0x7f030098;

    @AttrRes
    public static final int colorPrimary900 = 0x7f030099;

    @AttrRes
    public static final int colorPrimaryDark = 0x7f03009a;

    @AttrRes
    public static final int colorPrimaryLight = 0x7f03009b;

    @AttrRes
    public static final int colorSecondary = 0x7f03009c;

    @AttrRes
    public static final int colorSecondary100 = 0x7f03009d;

    @AttrRes
    public static final int colorSecondary200 = 0x7f03009e;

    @AttrRes
    public static final int colorSecondary300 = 0x7f03009f;

    @AttrRes
    public static final int colorSecondary400 = 0x7f0300a0;

    @AttrRes
    public static final int colorSecondary50 = 0x7f0300a1;

    @AttrRes
    public static final int colorSecondary500 = 0x7f0300a2;

    @AttrRes
    public static final int colorSecondary600 = 0x7f0300a3;

    @AttrRes
    public static final int colorSecondary700 = 0x7f0300a4;

    @AttrRes
    public static final int colorSecondary800 = 0x7f0300a5;

    @AttrRes
    public static final int colorSecondary900 = 0x7f0300a6;

    @AttrRes
    public static final int colorSecondaryDark = 0x7f0300a7;

    @AttrRes
    public static final int colorSecondaryLight = 0x7f0300a8;

    @AttrRes
    public static final int colorSwitchThumbNormal = 0x7f0300a9;

    @AttrRes
    public static final int commitIcon = 0x7f0300aa;

    @AttrRes
    public static final int contentDescription = 0x7f0300ab;

    @AttrRes
    public static final int contentInsetEnd = 0x7f0300ac;

    @AttrRes
    public static final int contentInsetEndWithActions = 0x7f0300ad;

    @AttrRes
    public static final int contentInsetLeft = 0x7f0300ae;

    @AttrRes
    public static final int contentInsetRight = 0x7f0300af;

    @AttrRes
    public static final int contentInsetStart = 0x7f0300b0;

    @AttrRes
    public static final int contentInsetStartWithNavigation = 0x7f0300b1;

    @AttrRes
    public static final int contentPadding = 0x7f0300b2;

    @AttrRes
    public static final int contentPaddingBottom = 0x7f0300b3;

    @AttrRes
    public static final int contentPaddingLeft = 0x7f0300b4;

    @AttrRes
    public static final int contentPaddingRight = 0x7f0300b5;

    @AttrRes
    public static final int contentPaddingTop = 0x7f0300b6;

    @AttrRes
    public static final int contentScrim = 0x7f0300b7;

    @AttrRes
    public static final int controlBackground = 0x7f0300b8;

    @AttrRes
    public static final int coordinatorLayoutStyle = 0x7f0300b9;

    @AttrRes
    public static final int cornerRadius = 0x7f0300ba;

    @AttrRes
    public static final int counterEnabled = 0x7f0300bb;

    @AttrRes
    public static final int counterMaxLength = 0x7f0300bc;

    @AttrRes
    public static final int counterOverflowTextAppearance = 0x7f0300bd;

    @AttrRes
    public static final int counterTextAppearance = 0x7f0300be;

    @AttrRes
    public static final int customNavigationLayout = 0x7f0300bf;

    @AttrRes
    public static final int defaultQueryHint = 0x7f0300c0;

    @AttrRes
    public static final int dialogCornerRadius = 0x7f0300c1;

    @AttrRes
    public static final int dialogPreferredPadding = 0x7f0300c2;

    @AttrRes
    public static final int dialogTheme = 0x7f0300c3;

    @AttrRes
    public static final int displayOptions = 0x7f0300c4;

    @AttrRes
    public static final int divider = 0x7f0300c5;

    @AttrRes
    public static final int dividerHorizontal = 0x7f0300c6;

    @AttrRes
    public static final int dividerPadding = 0x7f0300c7;

    @AttrRes
    public static final int dividerVertical = 0x7f0300c8;

    @AttrRes
    public static final int drawableSize = 0x7f0300c9;

    @AttrRes
    public static final int drawerArrowStyle = 0x7f0300ca;

    @AttrRes
    public static final int dropDownListViewStyle = 0x7f0300cb;

    @AttrRes
    public static final int dropdownListPreferredItemHeight = 0x7f0300cc;

    @AttrRes
    public static final int editTextBackground = 0x7f0300cd;

    @AttrRes
    public static final int editTextColor = 0x7f0300ce;

    @AttrRes
    public static final int editTextStyle = 0x7f0300cf;

    @AttrRes
    public static final int elevation = 0x7f0300d0;

    @AttrRes
    public static final int enforceMaterialTheme = 0x7f0300d1;

    @AttrRes
    public static final int errorEnabled = 0x7f0300d2;

    @AttrRes
    public static final int errorTextAppearance = 0x7f0300d3;

    @AttrRes
    public static final int expandActivityOverflowButtonDrawable = 0x7f0300d4;

    @AttrRes
    public static final int expanded = 0x7f0300d5;

    @AttrRes
    public static final int expandedTitleGravity = 0x7f0300d6;

    @AttrRes
    public static final int expandedTitleMargin = 0x7f0300d7;

    @AttrRes
    public static final int expandedTitleMarginBottom = 0x7f0300d8;

    @AttrRes
    public static final int expandedTitleMarginEnd = 0x7f0300d9;

    @AttrRes
    public static final int expandedTitleMarginStart = 0x7f0300da;

    @AttrRes
    public static final int expandedTitleMarginTop = 0x7f0300db;

    @AttrRes
    public static final int expandedTitleTextAppearance = 0x7f0300dc;

    @AttrRes
    public static final int fabAlignmentMode = 0x7f0300dd;

    @AttrRes
    public static final int fabAttached = 0x7f0300de;

    @AttrRes
    public static final int fabCradleDiameter = 0x7f0300df;

    @AttrRes
    public static final int fabCradleRoundedCornerRadius = 0x7f0300e0;

    @AttrRes
    public static final int fabCradleVerticalOffset = 0x7f0300e1;

    @AttrRes
    public static final int fabCustomSize = 0x7f0300e2;

    @AttrRes
    public static final int fabSize = 0x7f0300e3;

    @AttrRes
    public static final int fastScrollEnabled = 0x7f0300e4;

    @AttrRes
    public static final int fastScrollHorizontalThumbDrawable = 0x7f0300e5;

    @AttrRes
    public static final int fastScrollHorizontalTrackDrawable = 0x7f0300e6;

    @AttrRes
    public static final int fastScrollVerticalThumbDrawable = 0x7f0300e7;

    @AttrRes
    public static final int fastScrollVerticalTrackDrawable = 0x7f0300e8;

    @AttrRes
    public static final int floatingActionButtonStyle = 0x7f0300e9;

    @AttrRes
    public static final int font = 0x7f0300ea;

    @AttrRes
    public static final int fontFamily = 0x7f0300eb;

    @AttrRes
    public static final int fontProviderAuthority = 0x7f0300ec;

    @AttrRes
    public static final int fontProviderCerts = 0x7f0300ed;

    @AttrRes
    public static final int fontProviderFetchStrategy = 0x7f0300ee;

    @AttrRes
    public static final int fontProviderFetchTimeout = 0x7f0300ef;

    @AttrRes
    public static final int fontProviderPackage = 0x7f0300f0;

    @AttrRes
    public static final int fontProviderQuery = 0x7f0300f1;

    @AttrRes
    public static final int fontStyle = 0x7f0300f2;

    @AttrRes
    public static final int fontVariationSettings = 0x7f0300f3;

    @AttrRes
    public static final int fontWeight = 0x7f0300f4;

    @AttrRes
    public static final int foregroundInsidePadding = 0x7f0300f5;

    @AttrRes
    public static final int gapBetweenBars = 0x7f0300f6;

    @AttrRes
    public static final int goIcon = 0x7f0300f7;

    @AttrRes
    public static final int headerLayout = 0x7f0300f8;

    @AttrRes
    public static final int height = 0x7f0300f9;

    @AttrRes
    public static final int helperText = 0x7f0300fa;

    @AttrRes
    public static final int helperTextEnabled = 0x7f0300fb;

    @AttrRes
    public static final int helperTextTextAppearance = 0x7f0300fc;

    @AttrRes
    public static final int hideMotionSpec = 0x7f0300fd;

    @AttrRes
    public static final int hideOnContentScroll = 0x7f0300fe;

    @AttrRes
    public static final int hintAnimationEnabled = 0x7f0300ff;

    @AttrRes
    public static final int hintEnabled = 0x7f030100;

    @AttrRes
    public static final int hintTextAppearance = 0x7f030101;

    @AttrRes
    public static final int homeAsUpIndicator = 0x7f030102;

    @AttrRes
    public static final int homeLayout = 0x7f030103;

    @AttrRes
    public static final int hoveredFocusedTranslationZ = 0x7f030104;

    @AttrRes
    public static final int icon = 0x7f030105;

    @AttrRes
    public static final int iconEndPadding = 0x7f030106;

    @AttrRes
    public static final int iconPadding = 0x7f030107;

    @AttrRes
    public static final int iconStartPadding = 0x7f030108;

    @AttrRes
    public static final int iconTint = 0x7f030109;

    @AttrRes
    public static final int iconTintMode = 0x7f03010a;

    @AttrRes
    public static final int iconifiedByDefault = 0x7f03010b;

    @AttrRes
    public static final int imageButtonStyle = 0x7f03010c;

    @AttrRes
    public static final int indeterminateProgressStyle = 0x7f03010d;

    @AttrRes
    public static final int initialActivityCount = 0x7f03010e;

    @AttrRes
    public static final int insetForeground = 0x7f03010f;

    @AttrRes
    public static final int isLightTheme = 0x7f030110;

    @AttrRes
    public static final int itemBackground = 0x7f030111;

    @AttrRes
    public static final int itemHorizontalPadding = 0x7f030112;

    @AttrRes
    public static final int itemHorizontalTranslation = 0x7f030113;

    @AttrRes
    public static final int itemIconPadding = 0x7f030114;

    @AttrRes
    public static final int itemIconSize = 0x7f030115;

    @AttrRes
    public static final int itemIconTint = 0x7f030116;

    @AttrRes
    public static final int itemPadding = 0x7f030117;

    @AttrRes
    public static final int itemTextAppearance = 0x7f030118;

    @AttrRes
    public static final int itemTextAppearanceActive = 0x7f030119;

    @AttrRes
    public static final int itemTextAppearanceInactive = 0x7f03011a;

    @AttrRes
    public static final int itemTextColor = 0x7f03011b;

    @AttrRes
    public static final int keylines = 0x7f03011c;

    @AttrRes
    public static final int labelVisibilityMode = 0x7f03011d;

    @AttrRes
    public static final int layout = 0x7f03011e;

    @AttrRes
    public static final int layoutManager = 0x7f03011f;

    @AttrRes
    public static final int layout_anchor = 0x7f030120;

    @AttrRes
    public static final int layout_anchorGravity = 0x7f030121;

    @AttrRes
    public static final int layout_behavior = 0x7f030122;

    @AttrRes
    public static final int layout_collapseMode = 0x7f030123;

    @AttrRes
    public static final int layout_collapseParallaxMultiplier = 0x7f030124;

    @AttrRes
    public static final int layout_dodgeInsetEdges = 0x7f030125;

    @AttrRes
    public static final int layout_flexBasisPercent = 0x7f030126;

    @AttrRes
    public static final int layout_flexGrow = 0x7f030127;

    @AttrRes
    public static final int layout_flexShrink = 0x7f030128;

    @AttrRes
    public static final int layout_insetEdge = 0x7f030129;

    @AttrRes
    public static final int layout_keyline = 0x7f03012a;

    @AttrRes
    public static final int layout_maxHeight = 0x7f03012b;

    @AttrRes
    public static final int layout_maxWidth = 0x7f03012c;

    @AttrRes
    public static final int layout_minHeight = 0x7f03012d;

    @AttrRes
    public static final int layout_minWidth = 0x7f03012e;

    @AttrRes
    public static final int layout_order = 0x7f03012f;

    @AttrRes
    public static final int layout_scrollFlags = 0x7f030130;

    @AttrRes
    public static final int layout_scrollInterpolator = 0x7f030131;

    @AttrRes
    public static final int layout_wrapBefore = 0x7f030132;

    @AttrRes
    public static final int listChoiceBackgroundIndicator = 0x7f030133;

    @AttrRes
    public static final int listDividerAlertDialog = 0x7f030134;

    @AttrRes
    public static final int listItemLayout = 0x7f030135;

    @AttrRes
    public static final int listLayout = 0x7f030136;

    @AttrRes
    public static final int listMenuViewStyle = 0x7f030137;

    @AttrRes
    public static final int listPopupWindowStyle = 0x7f030138;

    @AttrRes
    public static final int listPreferredItemHeight = 0x7f030139;

    @AttrRes
    public static final int listPreferredItemHeightLarge = 0x7f03013a;

    @AttrRes
    public static final int listPreferredItemHeightSmall = 0x7f03013b;

    @AttrRes
    public static final int listPreferredItemPaddingLeft = 0x7f03013c;

    @AttrRes
    public static final int listPreferredItemPaddingRight = 0x7f03013d;

    @AttrRes
    public static final int logo = 0x7f03013e;

    @AttrRes
    public static final int logoDescription = 0x7f03013f;

    @AttrRes
    public static final int materialButtonStyle = 0x7f030140;

    @AttrRes
    public static final int materialCardViewStyle = 0x7f030141;

    @AttrRes
    public static final int materialSwitchStyle = 0x7f030142;

    @AttrRes
    public static final int maxActionInlineWidth = 0x7f030143;

    @AttrRes
    public static final int maxButtonHeight = 0x7f030144;

    @AttrRes
    public static final int maxImageSize = 0x7f030145;

    @AttrRes
    public static final int measureWithLargestChild = 0x7f030146;

    @AttrRes
    public static final int menu = 0x7f030147;

    @AttrRes
    public static final int multiChoiceItemLayout = 0x7f030148;

    @AttrRes
    public static final int navigationContentDescription = 0x7f030149;

    @AttrRes
    public static final int navigationIcon = 0x7f03014a;

    @AttrRes
    public static final int navigationMode = 0x7f03014b;

    @AttrRes
    public static final int navigationViewStyle = 0x7f03014c;

    @AttrRes
    public static final int numericModifiers = 0x7f03014d;

    @AttrRes
    public static final int overlapAnchor = 0x7f03014e;

    @AttrRes
    public static final int paddingBottomNoButtons = 0x7f03014f;

    @AttrRes
    public static final int paddingEnd = 0x7f030150;

    @AttrRes
    public static final int paddingStart = 0x7f030151;

    @AttrRes
    public static final int paddingTopNoTitle = 0x7f030152;

    @AttrRes
    public static final int panelBackground = 0x7f030153;

    @AttrRes
    public static final int panelMenuListTheme = 0x7f030154;

    @AttrRes
    public static final int panelMenuListWidth = 0x7f030155;

    @AttrRes
    public static final int passwordToggleContentDescription = 0x7f030156;

    @AttrRes
    public static final int passwordToggleDrawable = 0x7f030157;

    @AttrRes
    public static final int passwordToggleEnabled = 0x7f030158;

    @AttrRes
    public static final int passwordToggleTint = 0x7f030159;

    @AttrRes
    public static final int passwordToggleTintMode = 0x7f03015a;

    @AttrRes
    public static final int popupMenuStyle = 0x7f03015b;

    @AttrRes
    public static final int popupTheme = 0x7f03015c;

    @AttrRes
    public static final int popupWindowStyle = 0x7f03015d;

    @AttrRes
    public static final int preserveIconSpacing = 0x7f03015e;

    @AttrRes
    public static final int pressedTranslationZ = 0x7f03015f;

    @AttrRes
    public static final int progressBarPadding = 0x7f030160;

    @AttrRes
    public static final int progressBarStyle = 0x7f030161;

    @AttrRes
    public static final int queryBackground = 0x7f030162;

    @AttrRes
    public static final int queryHint = 0x7f030163;

    @AttrRes
    public static final int radioButtonStyle = 0x7f030164;

    @AttrRes
    public static final int ratingBarStyle = 0x7f030165;

    @AttrRes
    public static final int ratingBarStyleIndicator = 0x7f030166;

    @AttrRes
    public static final int ratingBarStyleSmall = 0x7f030167;

    @AttrRes
    public static final int reverseLayout = 0x7f030168;

    @AttrRes
    public static final int rippleColor = 0x7f030169;

    @AttrRes
    public static final int scrimAnimationDuration = 0x7f03016a;

    @AttrRes
    public static final int scrimBackground = 0x7f03016b;

    @AttrRes
    public static final int scrimVisibleHeightTrigger = 0x7f03016c;

    @AttrRes
    public static final int searchHintIcon = 0x7f03016d;

    @AttrRes
    public static final int searchIcon = 0x7f03016e;

    @AttrRes
    public static final int searchViewStyle = 0x7f03016f;

    @AttrRes
    public static final int seekBarStyle = 0x7f030170;

    @AttrRes
    public static final int selectableItemBackground = 0x7f030171;

    @AttrRes
    public static final int selectableItemBackgroundBorderless = 0x7f030172;

    @AttrRes
    public static final int showAsAction = 0x7f030173;

    @AttrRes
    public static final int showDividers = 0x7f030174;

    @AttrRes
    public static final int showMotionSpec = 0x7f030175;

    @AttrRes
    public static final int showText = 0x7f030176;

    @AttrRes
    public static final int showTitle = 0x7f030177;

    @AttrRes
    public static final int singleChoiceItemLayout = 0x7f030178;

    @AttrRes
    public static final int singleLine = 0x7f030179;

    @AttrRes
    public static final int singleSelection = 0x7f03017a;

    @AttrRes
    public static final int spanCount = 0x7f03017b;

    @AttrRes
    public static final int spinBars = 0x7f03017c;

    @AttrRes
    public static final int spinnerDropDownItemStyle = 0x7f03017d;

    @AttrRes
    public static final int spinnerStyle = 0x7f03017e;

    @AttrRes
    public static final int splitTrack = 0x7f03017f;

    @AttrRes
    public static final int srcCompat = 0x7f030180;

    @AttrRes
    public static final int stackFromEnd = 0x7f030181;

    @AttrRes
    public static final int state_above_anchor = 0x7f030182;

    @AttrRes
    public static final int state_collapsed = 0x7f030183;

    @AttrRes
    public static final int state_collapsible = 0x7f030184;

    @AttrRes
    public static final int statusBarBackground = 0x7f030185;

    @AttrRes
    public static final int statusBarScrim = 0x7f030186;

    @AttrRes
    public static final int strokeColor = 0x7f030187;

    @AttrRes
    public static final int strokeWidth = 0x7f030188;

    @AttrRes
    public static final int subMenuArrow = 0x7f030189;

    @AttrRes
    public static final int submitBackground = 0x7f03018a;

    @AttrRes
    public static final int subtitle = 0x7f03018b;

    @AttrRes
    public static final int subtitleTextAppearance = 0x7f03018c;

    @AttrRes
    public static final int subtitleTextColor = 0x7f03018d;

    @AttrRes
    public static final int subtitleTextStyle = 0x7f03018e;

    @AttrRes
    public static final int suggestionRowLayout = 0x7f03018f;

    @AttrRes
    public static final int switchMinWidth = 0x7f030190;

    @AttrRes
    public static final int switchPadding = 0x7f030191;

    @AttrRes
    public static final int switchStyle = 0x7f030192;

    @AttrRes
    public static final int switchTextAppearance = 0x7f030193;

    @AttrRes
    public static final int tabBackground = 0x7f030194;

    @AttrRes
    public static final int tabContentStart = 0x7f030195;

    @AttrRes
    public static final int tabGravity = 0x7f030196;

    @AttrRes
    public static final int tabIconTint = 0x7f030197;

    @AttrRes
    public static final int tabIconTintMode = 0x7f030198;

    @AttrRes
    public static final int tabIndicator = 0x7f030199;

    @AttrRes
    public static final int tabIndicatorColor = 0x7f03019a;

    @AttrRes
    public static final int tabIndicatorFullWidth = 0x7f03019b;

    @AttrRes
    public static final int tabIndicatorGravity = 0x7f03019c;

    @AttrRes
    public static final int tabIndicatorHeight = 0x7f03019d;

    @AttrRes
    public static final int tabInlineLabel = 0x7f03019e;

    @AttrRes
    public static final int tabMaxWidth = 0x7f03019f;

    @AttrRes
    public static final int tabMinWidth = 0x7f0301a0;

    @AttrRes
    public static final int tabMode = 0x7f0301a1;

    @AttrRes
    public static final int tabPadding = 0x7f0301a2;

    @AttrRes
    public static final int tabPaddingBottom = 0x7f0301a3;

    @AttrRes
    public static final int tabPaddingEnd = 0x7f0301a4;

    @AttrRes
    public static final int tabPaddingStart = 0x7f0301a5;

    @AttrRes
    public static final int tabPaddingTop = 0x7f0301a6;

    @AttrRes
    public static final int tabRippleColor = 0x7f0301a7;

    @AttrRes
    public static final int tabSelectedTextColor = 0x7f0301a8;

    @AttrRes
    public static final int tabStyle = 0x7f0301a9;

    @AttrRes
    public static final int tabTextAppearance = 0x7f0301aa;

    @AttrRes
    public static final int tabTextColor = 0x7f0301ab;

    @AttrRes
    public static final int tabUnboundedRipple = 0x7f0301ac;

    @AttrRes
    public static final int textAllCaps = 0x7f0301ad;

    @AttrRes
    public static final int textAppearanceBody1 = 0x7f0301ae;

    @AttrRes
    public static final int textAppearanceBody2 = 0x7f0301af;

    @AttrRes
    public static final int textAppearanceButton = 0x7f0301b0;

    @AttrRes
    public static final int textAppearanceCaption = 0x7f0301b1;

    @AttrRes
    public static final int textAppearanceHeadline1 = 0x7f0301b2;

    @AttrRes
    public static final int textAppearanceHeadline2 = 0x7f0301b3;

    @AttrRes
    public static final int textAppearanceHeadline3 = 0x7f0301b4;

    @AttrRes
    public static final int textAppearanceHeadline4 = 0x7f0301b5;

    @AttrRes
    public static final int textAppearanceHeadline5 = 0x7f0301b6;

    @AttrRes
    public static final int textAppearanceHeadline6 = 0x7f0301b7;

    @AttrRes
    public static final int textAppearanceLargePopupMenu = 0x7f0301b8;

    @AttrRes
    public static final int textAppearanceListItem = 0x7f0301b9;

    @AttrRes
    public static final int textAppearanceListItemSecondary = 0x7f0301ba;

    @AttrRes
    public static final int textAppearanceListItemSmall = 0x7f0301bb;

    @AttrRes
    public static final int textAppearanceOverline = 0x7f0301bc;

    @AttrRes
    public static final int textAppearancePopupMenuHeader = 0x7f0301bd;

    @AttrRes
    public static final int textAppearanceSearchResultSubtitle = 0x7f0301be;

    @AttrRes
    public static final int textAppearanceSearchResultTitle = 0x7f0301bf;

    @AttrRes
    public static final int textAppearanceSmallPopupMenu = 0x7f0301c0;

    @AttrRes
    public static final int textAppearanceSubtitle1 = 0x7f0301c1;

    @AttrRes
    public static final int textAppearanceSubtitle2 = 0x7f0301c2;

    @AttrRes
    public static final int textColorAlertDialogListItem = 0x7f0301c3;

    @AttrRes
    public static final int textColorSearchUrl = 0x7f0301c4;

    @AttrRes
    public static final int textEndPadding = 0x7f0301c5;

    @AttrRes
    public static final int textInputStyle = 0x7f0301c6;

    @AttrRes
    public static final int textStartPadding = 0x7f0301c7;

    @AttrRes
    public static final int theme = 0x7f0301c8;

    @AttrRes
    public static final int thickness = 0x7f0301c9;

    @AttrRes
    public static final int thumbTextPadding = 0x7f0301ca;

    @AttrRes
    public static final int thumbTint = 0x7f0301cb;

    @AttrRes
    public static final int thumbTintMode = 0x7f0301cc;

    @AttrRes
    public static final int tickMark = 0x7f0301cd;

    @AttrRes
    public static final int tickMarkTint = 0x7f0301ce;

    @AttrRes
    public static final int tickMarkTintMode = 0x7f0301cf;

    @AttrRes
    public static final int tint = 0x7f0301d0;

    @AttrRes
    public static final int tintMode = 0x7f0301d1;

    @AttrRes
    public static final int title = 0x7f0301d2;

    @AttrRes
    public static final int titleEnabled = 0x7f0301d3;

    @AttrRes
    public static final int titleMargin = 0x7f0301d4;

    @AttrRes
    public static final int titleMarginBottom = 0x7f0301d5;

    @AttrRes
    public static final int titleMarginEnd = 0x7f0301d6;

    @AttrRes
    public static final int titleMarginStart = 0x7f0301d7;

    @AttrRes
    public static final int titleMarginTop = 0x7f0301d8;

    @AttrRes
    public static final int titleMargins = 0x7f0301d9;

    @AttrRes
    public static final int titleTextAppearance = 0x7f0301da;

    @AttrRes
    public static final int titleTextColor = 0x7f0301db;

    @AttrRes
    public static final int titleTextStyle = 0x7f0301dc;

    @AttrRes
    public static final int toolbarId = 0x7f0301dd;

    @AttrRes
    public static final int toolbarNavigationButtonStyle = 0x7f0301de;

    @AttrRes
    public static final int toolbarStyle = 0x7f0301df;

    @AttrRes
    public static final int tooltipForegroundColor = 0x7f0301e0;

    @AttrRes
    public static final int tooltipFrameBackground = 0x7f0301e1;

    @AttrRes
    public static final int tooltipText = 0x7f0301e2;

    @AttrRes
    public static final int track = 0x7f0301e3;

    @AttrRes
    public static final int trackTint = 0x7f0301e4;

    @AttrRes
    public static final int trackTintMode = 0x7f0301e5;

    @AttrRes
    public static final int ttcIndex = 0x7f0301e6;

    @AttrRes
    public static final int useCompatPadding = 0x7f0301e7;

    @AttrRes
    public static final int viewInflaterClass = 0x7f0301e8;

    @AttrRes
    public static final int voiceIcon = 0x7f0301e9;

    @AttrRes
    public static final int windowActionBar = 0x7f0301ea;

    @AttrRes
    public static final int windowActionBarOverlay = 0x7f0301eb;

    @AttrRes
    public static final int windowActionModeOverlay = 0x7f0301ec;

    @AttrRes
    public static final int windowFixedHeightMajor = 0x7f0301ed;

    @AttrRes
    public static final int windowFixedHeightMinor = 0x7f0301ee;

    @AttrRes
    public static final int windowFixedWidthMajor = 0x7f0301ef;

    @AttrRes
    public static final int windowFixedWidthMinor = 0x7f0301f0;

    @AttrRes
    public static final int windowMinWidthMajor = 0x7f0301f1;

    @AttrRes
    public static final int windowMinWidthMinor = 0x7f0301f2;

    @AttrRes
    public static final int windowNoTitle = 0x7f0301f3;
  }

  public static final class bool {
    @BoolRes
    public static final int abc_action_bar_embed_tabs = 0x7f040000;

    @BoolRes
    public static final int abc_allow_stacked_button_bar = 0x7f040001;

    @BoolRes
    public static final int abc_config_actionMenuItemAllCaps = 0x7f040002;

    @BoolRes
    public static final int abc_config_showMenuShortcutsWhenKeyboardPresent = 0x7f040003;

    @BoolRes
    public static final int mtrl_btn_textappearance_all_caps = 0x7f040004;
  }

  public static final class color {
    @ColorRes
    public static final int abc_background_cache_hint_selector_material_dark = 0x7f050000;

    @ColorRes
    public static final int abc_background_cache_hint_selector_material_light = 0x7f050001;

    @ColorRes
    public static final int abc_btn_colored_borderless_text_material = 0x7f050002;

    @ColorRes
    public static final int abc_btn_colored_text_material = 0x7f050003;

    @ColorRes
    public static final int abc_color_highlight_material = 0x7f050004;

    @ColorRes
    public static final int abc_hint_foreground_material_dark = 0x7f050005;

    @ColorRes
    public static final int abc_hint_foreground_material_light = 0x7f050006;

    @ColorRes
    public static final int abc_input_method_navigation_guard = 0x7f050007;

    @ColorRes
    public static final int abc_primary_text_disable_only_material_dark = 0x7f050008;

    @ColorRes
    public static final int abc_primary_text_disable_only_material_light = 0x7f050009;

    @ColorRes
    public static final int abc_primary_text_material_dark = 0x7f05000a;

    @ColorRes
    public static final int abc_primary_text_material_light = 0x7f05000b;

    @ColorRes
    public static final int abc_search_url_text = 0x7f05000c;

    @ColorRes
    public static final int abc_search_url_text_normal = 0x7f05000d;

    @ColorRes
    public static final int abc_search_url_text_pressed = 0x7f05000e;

    @ColorRes
    public static final int abc_search_url_text_selected = 0x7f05000f;

    @ColorRes
    public static final int abc_secondary_text_material_dark = 0x7f050010;

    @ColorRes
    public static final int abc_secondary_text_material_light = 0x7f050011;

    @ColorRes
    public static final int abc_tint_btn_checkable = 0x7f050012;

    @ColorRes
    public static final int abc_tint_default = 0x7f050013;

    @ColorRes
    public static final int abc_tint_edittext = 0x7f050014;

    @ColorRes
    public static final int abc_tint_seek_thumb = 0x7f050015;

    @ColorRes
    public static final int abc_tint_spinner = 0x7f050016;

    @ColorRes
    public static final int abc_tint_switch_track = 0x7f050017;

    @ColorRes
    public static final int accent_material_dark = 0x7f050018;

    @ColorRes
    public static final int accent_material_light = 0x7f050019;

    @ColorRes
    public static final int background_floating_material_dark = 0x7f05001a;

    @ColorRes
    public static final int background_floating_material_light = 0x7f05001b;

    @ColorRes
    public static final int background_material_dark = 0x7f05001c;

    @ColorRes
    public static final int background_material_light = 0x7f05001d;

    @ColorRes
    public static final int bright_foreground_disabled_material_dark = 0x7f05001e;

    @ColorRes
    public static final int bright_foreground_disabled_material_light = 0x7f05001f;

    @ColorRes
    public static final int bright_foreground_inverse_material_dark = 0x7f050020;

    @ColorRes
    public static final int bright_foreground_inverse_material_light = 0x7f050021;

    @ColorRes
    public static final int bright_foreground_material_dark = 0x7f050022;

    @ColorRes
    public static final int bright_foreground_material_light = 0x7f050023;

    @ColorRes
    public static final int button_material_dark = 0x7f050024;

    @ColorRes
    public static final int button_material_light = 0x7f050025;

    @ColorRes
    public static final int cardview_dark_background = 0x7f050026;

    @ColorRes
    public static final int cardview_light_background = 0x7f050027;

    @ColorRes
    public static final int cardview_shadow_end_color = 0x7f050028;

    @ColorRes
    public static final int cardview_shadow_start_color = 0x7f050029;

    @ColorRes
    public static final int colorAccent = 0x7f05002a;

    @ColorRes
    public static final int colorPrimary = 0x7f05002b;

    @ColorRes
    public static final int colorPrimaryDark = 0x7f05002c;

    @ColorRes
    public static final int design_bottom_navigation_shadow_color = 0x7f05002d;

    @ColorRes
    public static final int design_default_color_primary = 0x7f05002e;

    @ColorRes
    public static final int design_default_color_primary_dark = 0x7f05002f;

    @ColorRes
    public static final int design_error = 0x7f050030;

    @ColorRes
    public static final int design_fab_shadow_end_color = 0x7f050031;

    @ColorRes
    public static final int design_fab_shadow_mid_color = 0x7f050032;

    @ColorRes
    public static final int design_fab_shadow_start_color = 0x7f050033;

    @ColorRes
    public static final int design_fab_stroke_end_inner_color = 0x7f050034;

    @ColorRes
    public static final int design_fab_stroke_end_outer_color = 0x7f050035;

    @ColorRes
    public static final int design_fab_stroke_top_inner_color = 0x7f050036;

    @ColorRes
    public static final int design_fab_stroke_top_outer_color = 0x7f050037;

    @ColorRes
    public static final int design_snackbar_background_color = 0x7f050038;

    @ColorRes
    public static final int design_tint_password_toggle = 0x7f050039;

    @ColorRes
    public static final int dim_foreground_disabled_material_dark = 0x7f05003a;

    @ColorRes
    public static final int dim_foreground_disabled_material_light = 0x7f05003b;

    @ColorRes
    public static final int dim_foreground_material_dark = 0x7f05003c;

    @ColorRes
    public static final int dim_foreground_material_light = 0x7f05003d;

    @ColorRes
    public static final int ecg_line = 0x7f05003e;

    @ColorRes
    public static final int ecg_line_bg_bold = 0x7f05003f;

    @ColorRes
    public static final int ecg_line_bg_normal = 0x7f050040;

    @ColorRes
    public static final int error_color_material_dark = 0x7f050041;

    @ColorRes
    public static final int error_color_material_light = 0x7f050042;

    @ColorRes
    public static final int foreground_material_dark = 0x7f050043;

    @ColorRes
    public static final int foreground_material_light = 0x7f050044;

    @ColorRes
    public static final int highlighted_text_material_dark = 0x7f050045;

    @ColorRes
    public static final int highlighted_text_material_light = 0x7f050046;

    @ColorRes
    public static final int material_blue_grey_800 = 0x7f050047;

    @ColorRes
    public static final int material_blue_grey_900 = 0x7f050048;

    @ColorRes
    public static final int material_blue_grey_950 = 0x7f050049;

    @ColorRes
    public static final int material_deep_teal_200 = 0x7f05004a;

    @ColorRes
    public static final int material_deep_teal_500 = 0x7f05004b;

    @ColorRes
    public static final int material_grey_100 = 0x7f05004c;

    @ColorRes
    public static final int material_grey_300 = 0x7f05004d;

    @ColorRes
    public static final int material_grey_50 = 0x7f05004e;

    @ColorRes
    public static final int material_grey_600 = 0x7f05004f;

    @ColorRes
    public static final int material_grey_800 = 0x7f050050;

    @ColorRes
    public static final int material_grey_850 = 0x7f050051;

    @ColorRes
    public static final int material_grey_900 = 0x7f050052;

    @ColorRes
    public static final int mtrl_bottom_nav_colored_item_tint = 0x7f050053;

    @ColorRes
    public static final int mtrl_bottom_nav_item_tint = 0x7f050054;

    @ColorRes
    public static final int mtrl_btn_bg_color_disabled = 0x7f050055;

    @ColorRes
    public static final int mtrl_btn_bg_color_selector = 0x7f050056;

    @ColorRes
    public static final int mtrl_btn_ripple_color = 0x7f050057;

    @ColorRes
    public static final int mtrl_btn_text_btn_ripple_color = 0x7f050058;

    @ColorRes
    public static final int mtrl_btn_text_color_disabled = 0x7f050059;

    @ColorRes
    public static final int mtrl_btn_text_color_selector = 0x7f05005a;

    @ColorRes
    public static final int mtrl_btn_transparent_bg_color = 0x7f05005b;

    @ColorRes
    public static final int mtrl_card_bg_color = 0x7f05005c;

    @ColorRes
    public static final int mtrl_chip_background_color = 0x7f05005d;

    @ColorRes
    public static final int mtrl_chip_close_icon_tint = 0x7f05005e;

    @ColorRes
    public static final int mtrl_chip_ripple_color = 0x7f05005f;

    @ColorRes
    public static final int mtrl_chip_text_color = 0x7f050060;

    @ColorRes
    public static final int mtrl_fab_ripple_color = 0x7f050061;

    @ColorRes
    public static final int mtrl_scrim_color = 0x7f050062;

    @ColorRes
    public static final int mtrl_tabs_colored_ripple_color = 0x7f050063;

    @ColorRes
    public static final int mtrl_tabs_icon_color_selector = 0x7f050064;

    @ColorRes
    public static final int mtrl_tabs_icon_color_selector_colored = 0x7f050065;

    @ColorRes
    public static final int mtrl_tabs_legacy_text_color_selector = 0x7f050066;

    @ColorRes
    public static final int mtrl_tabs_ripple_color = 0x7f050067;

    @ColorRes
    public static final int mtrl_text_btn_text_color_selector = 0x7f050068;

    @ColorRes
    public static final int mtrl_textinput_default_box_stroke_color = 0x7f050069;

    @ColorRes
    public static final int mtrl_textinput_disabled_color = 0x7f05006a;

    @ColorRes
    public static final int mtrl_textinput_filled_box_default_background_color = 0x7f05006b;

    @ColorRes
    public static final int mtrl_textinput_hovered_box_stroke_color = 0x7f05006c;

    @ColorRes
    public static final int notification_action_color_filter = 0x7f05006d;

    @ColorRes
    public static final int notification_icon_bg_color = 0x7f05006e;

    @ColorRes
    public static final int primary_dark_material_dark = 0x7f05006f;

    @ColorRes
    public static final int primary_dark_material_light = 0x7f050070;

    @ColorRes
    public static final int primary_material_dark = 0x7f050071;

    @ColorRes
    public static final int primary_material_light = 0x7f050072;

    @ColorRes
    public static final int primary_text_default_material_dark = 0x7f050073;

    @ColorRes
    public static final int primary_text_default_material_light = 0x7f050074;

    @ColorRes
    public static final int primary_text_disabled_material_dark = 0x7f050075;

    @ColorRes
    public static final int primary_text_disabled_material_light = 0x7f050076;

    @ColorRes
    public static final int ripple_material_dark = 0x7f050077;

    @ColorRes
    public static final int ripple_material_light = 0x7f050078;

    @ColorRes
    public static final int secondary_text_default_material_dark = 0x7f050079;

    @ColorRes
    public static final int secondary_text_default_material_light = 0x7f05007a;

    @ColorRes
    public static final int secondary_text_disabled_material_dark = 0x7f05007b;

    @ColorRes
    public static final int secondary_text_disabled_material_light = 0x7f05007c;

    @ColorRes
    public static final int switch_thumb_disabled_material_dark = 0x7f05007d;

    @ColorRes
    public static final int switch_thumb_disabled_material_light = 0x7f05007e;

    @ColorRes
    public static final int switch_thumb_material_dark = 0x7f05007f;

    @ColorRes
    public static final int switch_thumb_material_light = 0x7f050080;

    @ColorRes
    public static final int switch_thumb_normal_material_dark = 0x7f050081;

    @ColorRes
    public static final int switch_thumb_normal_material_light = 0x7f050082;

    @ColorRes
    public static final int tooltip_background_dark = 0x7f050083;

    @ColorRes
    public static final int tooltip_background_light = 0x7f050084;
  }

  public static final class dimen {
    @DimenRes
    public static final int abc_action_bar_content_inset_material = 0x7f060000;

    @DimenRes
    public static final int abc_action_bar_content_inset_with_nav = 0x7f060001;

    @DimenRes
    public static final int abc_action_bar_default_height_material = 0x7f060002;

    @DimenRes
    public static final int abc_action_bar_default_padding_end_material = 0x7f060003;

    @DimenRes
    public static final int abc_action_bar_default_padding_start_material = 0x7f060004;

    @DimenRes
    public static final int abc_action_bar_elevation_material = 0x7f060005;

    @DimenRes
    public static final int abc_action_bar_icon_vertical_padding_material = 0x7f060006;

    @DimenRes
    public static final int abc_action_bar_overflow_padding_end_material = 0x7f060007;

    @DimenRes
    public static final int abc_action_bar_overflow_padding_start_material = 0x7f060008;

    @DimenRes
    public static final int abc_action_bar_progress_bar_size = 0x7f060009;

    @DimenRes
    public static final int abc_action_bar_stacked_max_height = 0x7f06000a;

    @DimenRes
    public static final int abc_action_bar_stacked_tab_max_width = 0x7f06000b;

    @DimenRes
    public static final int abc_action_bar_subtitle_bottom_margin_material = 0x7f06000c;

    @DimenRes
    public static final int abc_action_bar_subtitle_top_margin_material = 0x7f06000d;

    @DimenRes
    public static final int abc_action_button_min_height_material = 0x7f06000e;

    @DimenRes
    public static final int abc_action_button_min_width_material = 0x7f06000f;

    @DimenRes
    public static final int abc_action_button_min_width_overflow_material = 0x7f060010;

    @DimenRes
    public static final int abc_alert_dialog_button_bar_height = 0x7f060011;

    @DimenRes
    public static final int abc_alert_dialog_button_dimen = 0x7f060012;

    @DimenRes
    public static final int abc_button_inset_horizontal_material = 0x7f060013;

    @DimenRes
    public static final int abc_button_inset_vertical_material = 0x7f060014;

    @DimenRes
    public static final int abc_button_padding_horizontal_material = 0x7f060015;

    @DimenRes
    public static final int abc_button_padding_vertical_material = 0x7f060016;

    @DimenRes
    public static final int abc_cascading_menus_min_smallest_width = 0x7f060017;

    @DimenRes
    public static final int abc_config_prefDialogWidth = 0x7f060018;

    @DimenRes
    public static final int abc_control_corner_material = 0x7f060019;

    @DimenRes
    public static final int abc_control_inset_material = 0x7f06001a;

    @DimenRes
    public static final int abc_control_padding_material = 0x7f06001b;

    @DimenRes
    public static final int abc_dialog_corner_radius_material = 0x7f06001c;

    @DimenRes
    public static final int abc_dialog_fixed_height_major = 0x7f06001d;

    @DimenRes
    public static final int abc_dialog_fixed_height_minor = 0x7f06001e;

    @DimenRes
    public static final int abc_dialog_fixed_width_major = 0x7f06001f;

    @DimenRes
    public static final int abc_dialog_fixed_width_minor = 0x7f060020;

    @DimenRes
    public static final int abc_dialog_list_padding_bottom_no_buttons = 0x7f060021;

    @DimenRes
    public static final int abc_dialog_list_padding_top_no_title = 0x7f060022;

    @DimenRes
    public static final int abc_dialog_min_width_major = 0x7f060023;

    @DimenRes
    public static final int abc_dialog_min_width_minor = 0x7f060024;

    @DimenRes
    public static final int abc_dialog_padding_material = 0x7f060025;

    @DimenRes
    public static final int abc_dialog_padding_top_material = 0x7f060026;

    @DimenRes
    public static final int abc_dialog_title_divider_material = 0x7f060027;

    @DimenRes
    public static final int abc_disabled_alpha_material_dark = 0x7f060028;

    @DimenRes
    public static final int abc_disabled_alpha_material_light = 0x7f060029;

    @DimenRes
    public static final int abc_dropdownitem_icon_width = 0x7f06002a;

    @DimenRes
    public static final int abc_dropdownitem_text_padding_left = 0x7f06002b;

    @DimenRes
    public static final int abc_dropdownitem_text_padding_right = 0x7f06002c;

    @DimenRes
    public static final int abc_edit_text_inset_bottom_material = 0x7f06002d;

    @DimenRes
    public static final int abc_edit_text_inset_horizontal_material = 0x7f06002e;

    @DimenRes
    public static final int abc_edit_text_inset_top_material = 0x7f06002f;

    @DimenRes
    public static final int abc_floating_window_z = 0x7f060030;

    @DimenRes
    public static final int abc_list_item_padding_horizontal_material = 0x7f060031;

    @DimenRes
    public static final int abc_panel_menu_list_width = 0x7f060032;

    @DimenRes
    public static final int abc_progress_bar_height_material = 0x7f060033;

    @DimenRes
    public static final int abc_search_view_preferred_height = 0x7f060034;

    @DimenRes
    public static final int abc_search_view_preferred_width = 0x7f060035;

    @DimenRes
    public static final int abc_seekbar_track_background_height_material = 0x7f060036;

    @DimenRes
    public static final int abc_seekbar_track_progress_height_material = 0x7f060037;

    @DimenRes
    public static final int abc_select_dialog_padding_start_material = 0x7f060038;

    @DimenRes
    public static final int abc_switch_padding = 0x7f060039;

    @DimenRes
    public static final int abc_text_size_body_1_material = 0x7f06003a;

    @DimenRes
    public static final int abc_text_size_body_2_material = 0x7f06003b;

    @DimenRes
    public static final int abc_text_size_button_material = 0x7f06003c;

    @DimenRes
    public static final int abc_text_size_caption_material = 0x7f06003d;

    @DimenRes
    public static final int abc_text_size_display_1_material = 0x7f06003e;

    @DimenRes
    public static final int abc_text_size_display_2_material = 0x7f06003f;

    @DimenRes
    public static final int abc_text_size_display_3_material = 0x7f060040;

    @DimenRes
    public static final int abc_text_size_display_4_material = 0x7f060041;

    @DimenRes
    public static final int abc_text_size_headline_material = 0x7f060042;

    @DimenRes
    public static final int abc_text_size_large_material = 0x7f060043;

    @DimenRes
    public static final int abc_text_size_medium_material = 0x7f060044;

    @DimenRes
    public static final int abc_text_size_menu_header_material = 0x7f060045;

    @DimenRes
    public static final int abc_text_size_menu_material = 0x7f060046;

    @DimenRes
    public static final int abc_text_size_small_material = 0x7f060047;

    @DimenRes
    public static final int abc_text_size_subhead_material = 0x7f060048;

    @DimenRes
    public static final int abc_text_size_subtitle_material_toolbar = 0x7f060049;

    @DimenRes
    public static final int abc_text_size_title_material = 0x7f06004a;

    @DimenRes
    public static final int abc_text_size_title_material_toolbar = 0x7f06004b;

    @DimenRes
    public static final int activity_horizontal_margin = 0x7f06004c;

    @DimenRes
    public static final int activity_vertical_margin = 0x7f06004d;

    @DimenRes
    public static final int cardview_compat_inset_shadow = 0x7f06004e;

    @DimenRes
    public static final int cardview_default_elevation = 0x7f06004f;

    @DimenRes
    public static final int cardview_default_radius = 0x7f060050;

    @DimenRes
    public static final int compat_button_inset_horizontal_material = 0x7f060051;

    @DimenRes
    public static final int compat_button_inset_vertical_material = 0x7f060052;

    @DimenRes
    public static final int compat_button_padding_horizontal_material = 0x7f060053;

    @DimenRes
    public static final int compat_button_padding_vertical_material = 0x7f060054;

    @DimenRes
    public static final int compat_control_corner_material = 0x7f060055;

    @DimenRes
    public static final int design_appbar_elevation = 0x7f060056;

    @DimenRes
    public static final int design_bottom_navigation_active_item_max_width = 0x7f060057;

    @DimenRes
    public static final int design_bottom_navigation_active_item_min_width = 0x7f060058;

    @DimenRes
    public static final int design_bottom_navigation_active_text_size = 0x7f060059;

    @DimenRes
    public static final int design_bottom_navigation_elevation = 0x7f06005a;

    @DimenRes
    public static final int design_bottom_navigation_height = 0x7f06005b;

    @DimenRes
    public static final int design_bottom_navigation_icon_size = 0x7f06005c;

    @DimenRes
    public static final int design_bottom_navigation_item_max_width = 0x7f06005d;

    @DimenRes
    public static final int design_bottom_navigation_item_min_width = 0x7f06005e;

    @DimenRes
    public static final int design_bottom_navigation_margin = 0x7f06005f;

    @DimenRes
    public static final int design_bottom_navigation_shadow_height = 0x7f060060;

    @DimenRes
    public static final int design_bottom_navigation_text_size = 0x7f060061;

    @DimenRes
    public static final int design_bottom_sheet_modal_elevation = 0x7f060062;

    @DimenRes
    public static final int design_bottom_sheet_peek_height_min = 0x7f060063;

    @DimenRes
    public static final int design_fab_border_width = 0x7f060064;

    @DimenRes
    public static final int design_fab_elevation = 0x7f060065;

    @DimenRes
    public static final int design_fab_image_size = 0x7f060066;

    @DimenRes
    public static final int design_fab_size_mini = 0x7f060067;

    @DimenRes
    public static final int design_fab_size_normal = 0x7f060068;

    @DimenRes
    public static final int design_fab_translation_z_hovered_focused = 0x7f060069;

    @DimenRes
    public static final int design_fab_translation_z_pressed = 0x7f06006a;

    @DimenRes
    public static final int design_navigation_elevation = 0x7f06006b;

    @DimenRes
    public static final int design_navigation_icon_padding = 0x7f06006c;

    @DimenRes
    public static final int design_navigation_icon_size = 0x7f06006d;

    @DimenRes
    public static final int design_navigation_item_horizontal_padding = 0x7f06006e;

    @DimenRes
    public static final int design_navigation_item_icon_padding = 0x7f06006f;

    @DimenRes
    public static final int design_navigation_max_width = 0x7f060070;

    @DimenRes
    public static final int design_navigation_padding_bottom = 0x7f060071;

    @DimenRes
    public static final int design_navigation_separator_vertical_padding = 0x7f060072;

    @DimenRes
    public static final int design_snackbar_action_inline_max_width = 0x7f060073;

    @DimenRes
    public static final int design_snackbar_background_corner_radius = 0x7f060074;

    @DimenRes
    public static final int design_snackbar_elevation = 0x7f060075;

    @DimenRes
    public static final int design_snackbar_extra_spacing_horizontal = 0x7f060076;

    @DimenRes
    public static final int design_snackbar_max_width = 0x7f060077;

    @DimenRes
    public static final int design_snackbar_min_width = 0x7f060078;

    @DimenRes
    public static final int design_snackbar_padding_horizontal = 0x7f060079;

    @DimenRes
    public static final int design_snackbar_padding_vertical = 0x7f06007a;

    @DimenRes
    public static final int design_snackbar_padding_vertical_2lines = 0x7f06007b;

    @DimenRes
    public static final int design_snackbar_text_size = 0x7f06007c;

    @DimenRes
    public static final int design_tab_max_width = 0x7f06007d;

    @DimenRes
    public static final int design_tab_scrollable_min_width = 0x7f06007e;

    @DimenRes
    public static final int design_tab_text_size = 0x7f06007f;

    @DimenRes
    public static final int design_tab_text_size_2line = 0x7f060080;

    @DimenRes
    public static final int design_textinput_caption_translate_y = 0x7f060081;

    @DimenRes
    public static final int disabled_alpha_material_dark = 0x7f060082;

    @DimenRes
    public static final int disabled_alpha_material_light = 0x7f060083;

    @DimenRes
    public static final int fab_margin = 0x7f060084;

    @DimenRes
    public static final int fastscroll_default_thickness = 0x7f060085;

    @DimenRes
    public static final int fastscroll_margin = 0x7f060086;

    @DimenRes
    public static final int fastscroll_minimum_range = 0x7f060087;

    @DimenRes
    public static final int highlight_alpha_material_colored = 0x7f060088;

    @DimenRes
    public static final int highlight_alpha_material_dark = 0x7f060089;

    @DimenRes
    public static final int highlight_alpha_material_light = 0x7f06008a;

    @DimenRes
    public static final int hint_alpha_material_dark = 0x7f06008b;

    @DimenRes
    public static final int hint_alpha_material_light = 0x7f06008c;

    @DimenRes
    public static final int hint_pressed_alpha_material_dark = 0x7f06008d;

    @DimenRes
    public static final int hint_pressed_alpha_material_light = 0x7f06008e;

    @DimenRes
    public static final int item_touch_helper_max_drag_scroll_per_frame = 0x7f06008f;

    @DimenRes
    public static final int item_touch_helper_swipe_escape_max_velocity = 0x7f060090;

    @DimenRes
    public static final int item_touch_helper_swipe_escape_velocity = 0x7f060091;

    @DimenRes
    public static final int mtrl_bottomappbar_fabOffsetEndMode = 0x7f060092;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_diameter = 0x7f060093;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_rounded_corner_radius = 0x7f060094;

    @DimenRes
    public static final int mtrl_bottomappbar_fab_cradle_vertical_offset = 0x7f060095;

    @DimenRes
    public static final int mtrl_bottomappbar_height = 0x7f060096;

    @DimenRes
    public static final int mtrl_btn_additional_padding_left_for_icon = 0x7f060097;

    @DimenRes
    public static final int mtrl_btn_additional_padding_right_for_icon = 0x7f060098;

    @DimenRes
    public static final int mtrl_btn_corner_radius = 0x7f060099;

    @DimenRes
    public static final int mtrl_btn_disabled_elevation = 0x7f06009a;

    @DimenRes
    public static final int mtrl_btn_disabled_z = 0x7f06009b;

    @DimenRes
    public static final int mtrl_btn_elevation = 0x7f06009c;

    @DimenRes
    public static final int mtrl_btn_focused_z = 0x7f06009d;

    @DimenRes
    public static final int mtrl_btn_hovered_z = 0x7f06009e;

    @DimenRes
    public static final int mtrl_btn_icon_padding = 0x7f06009f;

    @DimenRes
    public static final int mtrl_btn_inset = 0x7f0600a0;

    @DimenRes
    public static final int mtrl_btn_letter_spacing = 0x7f0600a1;

    @DimenRes
    public static final int mtrl_btn_padding_bottom = 0x7f0600a2;

    @DimenRes
    public static final int mtrl_btn_padding_left = 0x7f0600a3;

    @DimenRes
    public static final int mtrl_btn_padding_right = 0x7f0600a4;

    @DimenRes
    public static final int mtrl_btn_padding_top = 0x7f0600a5;

    @DimenRes
    public static final int mtrl_btn_pressed_z = 0x7f0600a6;

    @DimenRes
    public static final int mtrl_btn_stroke_size = 0x7f0600a7;

    @DimenRes
    public static final int mtrl_btn_text_btn_icon_padding = 0x7f0600a8;

    @DimenRes
    public static final int mtrl_btn_text_btn_padding_left = 0x7f0600a9;

    @DimenRes
    public static final int mtrl_btn_text_btn_padding_right = 0x7f0600aa;

    @DimenRes
    public static final int mtrl_btn_text_size = 0x7f0600ab;

    @DimenRes
    public static final int mtrl_btn_z = 0x7f0600ac;

    @DimenRes
    public static final int mtrl_card_elevation = 0x7f0600ad;

    @DimenRes
    public static final int mtrl_card_spacing = 0x7f0600ae;

    @DimenRes
    public static final int mtrl_chip_pressed_translation_z = 0x7f0600af;

    @DimenRes
    public static final int mtrl_chip_text_size = 0x7f0600b0;

    @DimenRes
    public static final int mtrl_fab_elevation = 0x7f0600b1;

    @DimenRes
    public static final int mtrl_fab_translation_z_hovered_focused = 0x7f0600b2;

    @DimenRes
    public static final int mtrl_fab_translation_z_pressed = 0x7f0600b3;

    @DimenRes
    public static final int mtrl_navigation_elevation = 0x7f0600b4;

    @DimenRes
    public static final int mtrl_navigation_item_horizontal_padding = 0x7f0600b5;

    @DimenRes
    public static final int mtrl_navigation_item_icon_padding = 0x7f0600b6;

    @DimenRes
    public static final int mtrl_textinput_box_bottom_offset = 0x7f0600b7;

    @DimenRes
    public static final int mtrl_textinput_box_corner_radius_medium = 0x7f0600b8;

    @DimenRes
    public static final int mtrl_textinput_box_corner_radius_small = 0x7f0600b9;

    @DimenRes
    public static final int mtrl_textinput_box_label_cutout_padding = 0x7f0600ba;

    @DimenRes
    public static final int mtrl_textinput_box_padding_bottom = 0x7f0600bb;

    @DimenRes
    public static final int mtrl_textinput_box_padding_left = 0x7f0600bc;

    @DimenRes
    public static final int mtrl_textinput_box_padding_right = 0x7f0600bd;

    @DimenRes
    public static final int mtrl_textinput_box_stroke_width_default = 0x7f0600be;

    @DimenRes
    public static final int mtrl_textinput_box_stroke_width_focused = 0x7f0600bf;

    @DimenRes
    public static final int mtrl_textinput_filled_box_collapsed_padding = 0x7f0600c0;

    @DimenRes
    public static final int mtrl_textinput_filled_box_collapsed_padding_dense = 0x7f0600c1;

    @DimenRes
    public static final int mtrl_textinput_filled_box_expanded_padding = 0x7f0600c2;

    @DimenRes
    public static final int mtrl_textinput_filled_box_expanded_padding_dense = 0x7f0600c3;

    @DimenRes
    public static final int mtrl_textinput_outline_box_collapsed_padding_bottom = 0x7f0600c4;

    @DimenRes
    public static final int mtrl_textinput_outline_box_collapsed_padding_dense = 0x7f0600c5;

    @DimenRes
    public static final int mtrl_textinput_outline_box_collapsed_padding_top = 0x7f0600c6;

    @DimenRes
    public static final int mtrl_textinput_outline_box_expanded_padding = 0x7f0600c7;

    @DimenRes
    public static final int mtrl_textinput_outline_box_expanded_padding_dense = 0x7f0600c8;

    @DimenRes
    public static final int notification_action_icon_size = 0x7f0600c9;

    @DimenRes
    public static final int notification_action_text_size = 0x7f0600ca;

    @DimenRes
    public static final int notification_big_circle_margin = 0x7f0600cb;

    @DimenRes
    public static final int notification_content_margin_start = 0x7f0600cc;

    @DimenRes
    public static final int notification_large_icon_height = 0x7f0600cd;

    @DimenRes
    public static final int notification_large_icon_width = 0x7f0600ce;

    @DimenRes
    public static final int notification_main_column_padding_top = 0x7f0600cf;

    @DimenRes
    public static final int notification_media_narrow_margin = 0x7f0600d0;

    @DimenRes
    public static final int notification_right_icon_size = 0x7f0600d1;

    @DimenRes
    public static final int notification_right_side_padding_top = 0x7f0600d2;

    @DimenRes
    public static final int notification_small_icon_background_padding = 0x7f0600d3;

    @DimenRes
    public static final int notification_small_icon_size_as_large = 0x7f0600d4;

    @DimenRes
    public static final int notification_subtext_size = 0x7f0600d5;

    @DimenRes
    public static final int notification_top_pad = 0x7f0600d6;

    @DimenRes
    public static final int notification_top_pad_large_text = 0x7f0600d7;

    @DimenRes
    public static final int tooltip_corner_radius = 0x7f0600d8;

    @DimenRes
    public static final int tooltip_horizontal_padding = 0x7f0600d9;

    @DimenRes
    public static final int tooltip_margin = 0x7f0600da;

    @DimenRes
    public static final int tooltip_precise_anchor_extra_offset = 0x7f0600db;

    @DimenRes
    public static final int tooltip_precise_anchor_threshold = 0x7f0600dc;

    @DimenRes
    public static final int tooltip_vertical_padding = 0x7f0600dd;

    @DimenRes
    public static final int tooltip_y_offset_non_touch = 0x7f0600de;

    @DimenRes
    public static final int tooltip_y_offset_touch = 0x7f0600df;
  }

  public static final class drawable {
    @DrawableRes
    public static final int abc_ab_share_pack_mtrl_alpha = 0x7f070006;

    @DrawableRes
    public static final int abc_action_bar_item_background_material = 0x7f070007;

    @DrawableRes
    public static final int abc_btn_borderless_material = 0x7f070008;

    @DrawableRes
    public static final int abc_btn_check_material = 0x7f070009;

    @DrawableRes
    public static final int abc_btn_check_to_on_mtrl_000 = 0x7f07000a;

    @DrawableRes
    public static final int abc_btn_check_to_on_mtrl_015 = 0x7f07000b;

    @DrawableRes
    public static final int abc_btn_colored_material = 0x7f07000c;

    @DrawableRes
    public static final int abc_btn_default_mtrl_shape = 0x7f07000d;

    @DrawableRes
    public static final int abc_btn_radio_material = 0x7f07000e;

    @DrawableRes
    public static final int abc_btn_radio_to_on_mtrl_000 = 0x7f07000f;

    @DrawableRes
    public static final int abc_btn_radio_to_on_mtrl_015 = 0x7f070010;

    @DrawableRes
    public static final int abc_btn_switch_to_on_mtrl_00001 = 0x7f070011;

    @DrawableRes
    public static final int abc_btn_switch_to_on_mtrl_00012 = 0x7f070012;

    @DrawableRes
    public static final int abc_cab_background_internal_bg = 0x7f070013;

    @DrawableRes
    public static final int abc_cab_background_top_material = 0x7f070014;

    @DrawableRes
    public static final int abc_cab_background_top_mtrl_alpha = 0x7f070015;

    @DrawableRes
    public static final int abc_control_background_material = 0x7f070016;

    @DrawableRes
    public static final int abc_dialog_material_background = 0x7f070017;

    @DrawableRes
    public static final int abc_edit_text_material = 0x7f070018;

    @DrawableRes
    public static final int abc_ic_ab_back_material = 0x7f070019;

    @DrawableRes
    public static final int abc_ic_arrow_drop_right_black_24dp = 0x7f07001a;

    @DrawableRes
    public static final int abc_ic_clear_material = 0x7f07001b;

    @DrawableRes
    public static final int abc_ic_commit_search_api_mtrl_alpha = 0x7f07001c;

    @DrawableRes
    public static final int abc_ic_go_search_api_material = 0x7f07001d;

    @DrawableRes
    public static final int abc_ic_menu_copy_mtrl_am_alpha = 0x7f07001e;

    @DrawableRes
    public static final int abc_ic_menu_cut_mtrl_alpha = 0x7f07001f;

    @DrawableRes
    public static final int abc_ic_menu_overflow_material = 0x7f070020;

    @DrawableRes
    public static final int abc_ic_menu_paste_mtrl_am_alpha = 0x7f070021;

    @DrawableRes
    public static final int abc_ic_menu_selectall_mtrl_alpha = 0x7f070022;

    @DrawableRes
    public static final int abc_ic_menu_share_mtrl_alpha = 0x7f070023;

    @DrawableRes
    public static final int abc_ic_search_api_material = 0x7f070024;

    @DrawableRes
    public static final int abc_ic_star_black_16dp = 0x7f070025;

    @DrawableRes
    public static final int abc_ic_star_black_36dp = 0x7f070026;

    @DrawableRes
    public static final int abc_ic_star_black_48dp = 0x7f070027;

    @DrawableRes
    public static final int abc_ic_star_half_black_16dp = 0x7f070028;

    @DrawableRes
    public static final int abc_ic_star_half_black_36dp = 0x7f070029;

    @DrawableRes
    public static final int abc_ic_star_half_black_48dp = 0x7f07002a;

    @DrawableRes
    public static final int abc_ic_voice_search_api_material = 0x7f07002b;

    @DrawableRes
    public static final int abc_item_background_holo_dark = 0x7f07002c;

    @DrawableRes
    public static final int abc_item_background_holo_light = 0x7f07002d;

    @DrawableRes
    public static final int abc_list_divider_material = 0x7f07002e;

    @DrawableRes
    public static final int abc_list_divider_mtrl_alpha = 0x7f07002f;

    @DrawableRes
    public static final int abc_list_focused_holo = 0x7f070030;

    @DrawableRes
    public static final int abc_list_longpressed_holo = 0x7f070031;

    @DrawableRes
    public static final int abc_list_pressed_holo_dark = 0x7f070032;

    @DrawableRes
    public static final int abc_list_pressed_holo_light = 0x7f070033;

    @DrawableRes
    public static final int abc_list_selector_background_transition_holo_dark = 0x7f070034;

    @DrawableRes
    public static final int abc_list_selector_background_transition_holo_light = 0x7f070035;

    @DrawableRes
    public static final int abc_list_selector_disabled_holo_dark = 0x7f070036;

    @DrawableRes
    public static final int abc_list_selector_disabled_holo_light = 0x7f070037;

    @DrawableRes
    public static final int abc_list_selector_holo_dark = 0x7f070038;

    @DrawableRes
    public static final int abc_list_selector_holo_light = 0x7f070039;

    @DrawableRes
    public static final int abc_menu_hardkey_panel_mtrl_mult = 0x7f07003a;

    @DrawableRes
    public static final int abc_popup_background_mtrl_mult = 0x7f07003b;

    @DrawableRes
    public static final int abc_ratingbar_indicator_material = 0x7f07003c;

    @DrawableRes
    public static final int abc_ratingbar_material = 0x7f07003d;

    @DrawableRes
    public static final int abc_ratingbar_small_material = 0x7f07003e;

    @DrawableRes
    public static final int abc_scrubber_control_off_mtrl_alpha = 0x7f07003f;

    @DrawableRes
    public static final int abc_scrubber_control_to_pressed_mtrl_000 = 0x7f070040;

    @DrawableRes
    public static final int abc_scrubber_control_to_pressed_mtrl_005 = 0x7f070041;

    @DrawableRes
    public static final int abc_scrubber_primary_mtrl_alpha = 0x7f070042;

    @DrawableRes
    public static final int abc_scrubber_track_mtrl_alpha = 0x7f070043;

    @DrawableRes
    public static final int abc_seekbar_thumb_material = 0x7f070044;

    @DrawableRes
    public static final int abc_seekbar_tick_mark_material = 0x7f070045;

    @DrawableRes
    public static final int abc_seekbar_track_material = 0x7f070046;

    @DrawableRes
    public static final int abc_spinner_mtrl_am_alpha = 0x7f070047;

    @DrawableRes
    public static final int abc_spinner_textfield_background_material = 0x7f070048;

    @DrawableRes
    public static final int abc_switch_thumb_material = 0x7f070049;

    @DrawableRes
    public static final int abc_switch_track_mtrl_alpha = 0x7f07004a;

    @DrawableRes
    public static final int abc_tab_indicator_material = 0x7f07004b;

    @DrawableRes
    public static final int abc_tab_indicator_mtrl_alpha = 0x7f07004c;

    @DrawableRes
    public static final int abc_text_cursor_material = 0x7f07004d;

    @DrawableRes
    public static final int abc_text_select_handle_left_mtrl_dark = 0x7f07004e;

    @DrawableRes
    public static final int abc_text_select_handle_left_mtrl_light = 0x7f07004f;

    @DrawableRes
    public static final int abc_text_select_handle_middle_mtrl_dark = 0x7f070050;

    @DrawableRes
    public static final int abc_text_select_handle_middle_mtrl_light = 0x7f070051;

    @DrawableRes
    public static final int abc_text_select_handle_right_mtrl_dark = 0x7f070052;

    @DrawableRes
    public static final int abc_text_select_handle_right_mtrl_light = 0x7f070053;

    @DrawableRes
    public static final int abc_textfield_activated_mtrl_alpha = 0x7f070054;

    @DrawableRes
    public static final int abc_textfield_default_mtrl_alpha = 0x7f070055;

    @DrawableRes
    public static final int abc_textfield_search_activated_mtrl_alpha = 0x7f070056;

    @DrawableRes
    public static final int abc_textfield_search_default_mtrl_alpha = 0x7f070057;

    @DrawableRes
    public static final int abc_textfield_search_material = 0x7f070058;

    @DrawableRes
    public static final int abc_vector_test = 0x7f070059;

    @DrawableRes
    public static final int avd_hide_password = 0x7f07005a;

    @DrawableRes
    public static final int avd_show_password = 0x7f07005b;

    @DrawableRes
    public static final int design_bottom_navigation_item_background = 0x7f07005c;

    @DrawableRes
    public static final int design_fab_background = 0x7f07005d;

    @DrawableRes
    public static final int design_ic_visibility = 0x7f07005e;

    @DrawableRes
    public static final int design_ic_visibility_off = 0x7f07005f;

    @DrawableRes
    public static final int design_password_eye = 0x7f070060;

    @DrawableRes
    public static final int design_snackbar_background = 0x7f070061;

    @DrawableRes
    public static final int ic_action_notify_cancel = 0x7f070062;

    @DrawableRes
    public static final int ic_launcher_background = 0x7f070063;

    @DrawableRes
    public static final int ic_mtrl_chip_checked_black = 0x7f070064;

    @DrawableRes
    public static final int ic_mtrl_chip_checked_circle = 0x7f070065;

    @DrawableRes
    public static final int ic_mtrl_chip_close_circle = 0x7f070066;

    @DrawableRes
    public static final int login_button = 0x7f070067;

    @DrawableRes
    public static final int login_button_normal = 0x7f070068;

    @DrawableRes
    public static final int login_button_press = 0x7f070069;

    @DrawableRes
    public static final int login_button_press1 = 0x7f07006a;

    @DrawableRes
    public static final int mtrl_tabs_default_indicator = 0x7f07006b;

    @DrawableRes
    public static final int navigation_empty_icon = 0x7f07006c;

    @DrawableRes
    public static final int notification_action_background = 0x7f07006d;

    @DrawableRes
    public static final int notification_bg = 0x7f07006e;

    @DrawableRes
    public static final int notification_bg_low = 0x7f07006f;

    @DrawableRes
    public static final int notification_bg_low_normal = 0x7f070070;

    @DrawableRes
    public static final int notification_bg_low_pressed = 0x7f070071;

    @DrawableRes
    public static final int notification_bg_normal = 0x7f070072;

    @DrawableRes
    public static final int notification_bg_normal_pressed = 0x7f070073;

    @DrawableRes
    public static final int notification_icon_background = 0x7f070074;

    @DrawableRes
    public static final int notification_template_icon_bg = 0x7f070075;

    @DrawableRes
    public static final int notification_template_icon_low_bg = 0x7f070076;

    @DrawableRes
    public static final int notification_tile_bg = 0x7f070077;

    @DrawableRes
    public static final int notify_panel_notification_icon_bg = 0x7f070078;

    @DrawableRes
    public static final int refresh = 0x7f070079;

    @DrawableRes
    public static final int tooltip_frame_dark = 0x7f07007a;

    @DrawableRes
    public static final int tooltip_frame_light = 0x7f07007b;
  }

  public static final class id {
    @IdRes
    public static final int ALT = 0x7f080000;

    @IdRes
    public static final int CTRL = 0x7f080001;

    @IdRes
    public static final int FUNCTION = 0x7f080002;

    @IdRes
    public static final int META = 0x7f080003;

    @IdRes
    public static final int SHIFT = 0x7f080004;

    @IdRes
    public static final int SYM = 0x7f080005;

    @IdRes
    public static final int action_bar = 0x7f080006;

    @IdRes
    public static final int action_bar_activity_content = 0x7f080007;

    @IdRes
    public static final int action_bar_container = 0x7f080008;

    @IdRes
    public static final int action_bar_root = 0x7f080009;

    @IdRes
    public static final int action_bar_spinner = 0x7f08000a;

    @IdRes
    public static final int action_bar_subtitle = 0x7f08000b;

    @IdRes
    public static final int action_bar_title = 0x7f08000c;

    @IdRes
    public static final int action_container = 0x7f08000d;

    @IdRes
    public static final int action_context_bar = 0x7f08000e;

    @IdRes
    public static final int action_divider = 0x7f08000f;

    @IdRes
    public static final int action_image = 0x7f080010;

    @IdRes
    public static final int action_menu_divider = 0x7f080011;

    @IdRes
    public static final int action_menu_presenter = 0x7f080012;

    @IdRes
    public static final int action_mode_bar = 0x7f080013;

    @IdRes
    public static final int action_mode_bar_stub = 0x7f080014;

    @IdRes
    public static final int action_mode_close_button = 0x7f080015;

    @IdRes
    public static final int action_settings = 0x7f080016;

    @IdRes
    public static final int action_text = 0x7f080017;

    @IdRes
    public static final int actions = 0x7f080018;

    @IdRes
    public static final int activity_chooser_view_content = 0x7f080019;

    @IdRes
    public static final int add = 0x7f08001a;

    @IdRes
    public static final int aimsportcalc_head = 0x7f08001b;

    @IdRes
    public static final int aimsportcalc_person_height = 0x7f08001c;

    @IdRes
    public static final int aimsportcalc_person_sex = 0x7f08001d;

    @IdRes
    public static final int aimsportcalc_person_sexm = 0x7f08001e;

    @IdRes
    public static final int aimsportcalc_person_sexw = 0x7f08001f;

    @IdRes
    public static final int aimsportcalc_person_weight = 0x7f080020;

    @IdRes
    public static final int aimsportcalc_steps = 0x7f080021;

    @IdRes
    public static final int aimsportcalc_tv = 0x7f080022;

    @IdRes
    public static final int aimsportcalc_update = 0x7f080023;

    @IdRes
    public static final int alertTitle = 0x7f080024;

    @IdRes
    public static final int all = 0x7f080025;

    @IdRes
    public static final int always = 0x7f080026;

    @IdRes
    public static final int async = 0x7f080027;

    @IdRes
    public static final int auto = 0x7f080028;

    @IdRes
    public static final int beginning = 0x7f080029;

    @IdRes
    public static final int blocking = 0x7f08002a;

    @IdRes
    public static final int bottom = 0x7f08002b;

    @IdRes
    public static final int buttonPanel = 0x7f08002c;

    @IdRes
    public static final int center = 0x7f08002d;

    @IdRes
    public static final int center_horizontal = 0x7f08002e;

    @IdRes
    public static final int center_vertical = 0x7f08002f;

    @IdRes
    public static final int checkbox = 0x7f080030;

    @IdRes
    public static final int chronometer = 0x7f080031;

    @IdRes
    public static final int clip_horizontal = 0x7f080032;

    @IdRes
    public static final int clip_vertical = 0x7f080033;

    @IdRes
    public static final int collapseActionView = 0x7f080034;

    @IdRes
    public static final int container = 0x7f080035;

    @IdRes
    public static final int content = 0x7f080036;

    @IdRes
    public static final int contentPanel = 0x7f080037;

    @IdRes
    public static final int coordinator = 0x7f080038;

    @IdRes
    public static final int custom = 0x7f080039;

    @IdRes
    public static final int customPanel = 0x7f08003a;

    @IdRes
    public static final int decor_content_parent = 0x7f08003b;

    @IdRes
    public static final int default_activity_button = 0x7f08003c;

    @IdRes
    public static final int design_bottom_sheet = 0x7f08003d;

    @IdRes
    public static final int design_menu_item_action_area = 0x7f08003e;

    @IdRes
    public static final int design_menu_item_action_area_stub = 0x7f08003f;

    @IdRes
    public static final int design_menu_item_text = 0x7f080040;

    @IdRes
    public static final int design_navigation_view = 0x7f080041;

    @IdRes
    public static final int disableHome = 0x7f080042;

    @IdRes
    public static final int ecg_real_view = 0x7f080043;

    @IdRes
    public static final int ecg_start = 0x7f080044;

    @IdRes
    public static final int ecg_stop = 0x7f080045;

    @IdRes
    public static final int edit_query = 0x7f080046;

    @IdRes
    public static final int end = 0x7f080047;

    @IdRes
    public static final int enterAlways = 0x7f080048;

    @IdRes
    public static final int enterAlwaysCollapsed = 0x7f080049;

    @IdRes
    public static final int exitUntilCollapsed = 0x7f08004a;

    @IdRes
    public static final int expand_activities_button = 0x7f08004b;

    @IdRes
    public static final int expanded_menu = 0x7f08004c;

    @IdRes
    public static final int fill = 0x7f08004d;

    @IdRes
    public static final int fill_horizontal = 0x7f08004e;

    @IdRes
    public static final int fill_vertical = 0x7f08004f;

    @IdRes
    public static final int filled = 0x7f080050;

    @IdRes
    public static final int fixed = 0x7f080051;

    @IdRes
    public static final int forever = 0x7f080052;

    @IdRes
    public static final int ghost_view = 0x7f080053;

    @IdRes
    public static final int greenlightdata = 0x7f080054;

    @IdRes
    public static final int gridbutton = 0x7f080055;

    @IdRes
    public static final int group_divider = 0x7f080056;

    @IdRes
    public static final int home = 0x7f080057;

    @IdRes
    public static final int homeAsUp = 0x7f080058;

    @IdRes
    public static final int icon = 0x7f080059;

    @IdRes
    public static final int icon_group = 0x7f08005a;

    @IdRes
    public static final int ifRoom = 0x7f08005b;

    @IdRes
    public static final int image = 0x7f08005c;

    @IdRes
    public static final int info = 0x7f08005d;

    @IdRes
    public static final int institution_distance = 0x7f08005e;

    @IdRes
    public static final int institution_head = 0x7f08005f;

    @IdRes
    public static final int institution_person_height = 0x7f080060;

    @IdRes
    public static final int institution_person_weight = 0x7f080061;

    @IdRes
    public static final int institution_tv = 0x7f080062;

    @IdRes
    public static final int institution_update = 0x7f080063;

    @IdRes
    public static final int italic = 0x7f080064;

    @IdRes
    public static final int item_touch_helper_previous_elevation = 0x7f080065;

    @IdRes
    public static final int labeled = 0x7f080066;

    @IdRes
    public static final int largeLabel = 0x7f080067;

    @IdRes
    public static final int left = 0x7f080068;

    @IdRes
    public static final int line1 = 0x7f080069;

    @IdRes
    public static final int line3 = 0x7f08006a;

    @IdRes
    public static final int listMode = 0x7f08006b;

    @IdRes
    public static final int list_item = 0x7f08006c;

    @IdRes
    public static final int main_gridview = 0x7f08006d;

    @IdRes
    public static final int main_head = 0x7f08006e;

    @IdRes
    public static final int main_title = 0x7f08006f;

    @IdRes
    public static final int masked = 0x7f080070;

    @IdRes
    public static final int message = 0x7f080071;

    @IdRes
    public static final int middle = 0x7f080072;

    @IdRes
    public static final int mini = 0x7f080073;

    @IdRes
    public static final int mtrl_child_content_container = 0x7f080074;

    @IdRes
    public static final int mtrl_internal_children_alpha_tag = 0x7f080075;

    @IdRes
    public static final int multiply = 0x7f080076;

    @IdRes
    public static final int navigation_header_container = 0x7f080077;

    @IdRes
    public static final int never = 0x7f080078;

    @IdRes
    public static final int none = 0x7f080079;

    @IdRes
    public static final int normal = 0x7f08007a;

    @IdRes
    public static final int notification_background = 0x7f08007b;

    @IdRes
    public static final int notification_main_column = 0x7f08007c;

    @IdRes
    public static final int notification_main_column_container = 0x7f08007d;

    @IdRes
    public static final int oad_descripe = 0x7f08007e;

    @IdRes
    public static final int oad_devicestate = 0x7f08007f;

    @IdRes
    public static final int oad_localversion = 0x7f080080;

    @IdRes
    public static final int oad_netversion = 0x7f080081;

    @IdRes
    public static final int oad_update = 0x7f080082;

    @IdRes
    public static final int oad_upload_progress = 0x7f080083;

    @IdRes
    public static final int oad_upload_progress_gv = 0x7f080084;

    @IdRes
    public static final int oad_uploading = 0x7f080085;

    @IdRes
    public static final int outline = 0x7f080086;

    @IdRes
    public static final int parallax = 0x7f080087;

    @IdRes
    public static final int parentPanel = 0x7f080088;

    @IdRes
    public static final int parent_matrix = 0x7f080089;

    @IdRes
    public static final int pin = 0x7f08008a;

    @IdRes
    public static final int progress_circular = 0x7f08008b;

    @IdRes
    public static final int progress_horizontal = 0x7f08008c;

    @IdRes
    public static final int ptt_model = 0x7f08008d;

    @IdRes
    public static final int ptt_real_view = 0x7f08008e;

    @IdRes
    public static final int ptt_sign_close = 0x7f08008f;

    @IdRes
    public static final int ptt_sign_open = 0x7f080090;

    @IdRes
    public static final int radio = 0x7f080091;

    @IdRes
    public static final int right = 0x7f080092;

    @IdRes
    public static final int right_icon = 0x7f080093;

    @IdRes
    public static final int right_side = 0x7f080094;

    @IdRes
    public static final int save_image_matrix = 0x7f080095;

    @IdRes
    public static final int save_non_transition_alpha = 0x7f080096;

    @IdRes
    public static final int save_scale_type = 0x7f080097;

    @IdRes
    public static final int scan = 0x7f080098;

    @IdRes
    public static final int screen = 0x7f080099;

    @IdRes
    public static final int scroll = 0x7f08009a;

    @IdRes
    public static final int scrollIndicatorDown = 0x7f08009b;

    @IdRes
    public static final int scrollIndicatorUp = 0x7f08009c;

    @IdRes
    public static final int scrollView = 0x7f08009d;

    @IdRes
    public static final int scrollable = 0x7f08009e;

    @IdRes
    public static final int search_badge = 0x7f08009f;

    @IdRes
    public static final int search_bar = 0x7f0800a0;

    @IdRes
    public static final int search_button = 0x7f0800a1;

    @IdRes
    public static final int search_close_btn = 0x7f0800a2;

    @IdRes
    public static final int search_edit_frame = 0x7f0800a3;

    @IdRes
    public static final int search_go_btn = 0x7f0800a4;

    @IdRes
    public static final int search_mag_icon = 0x7f0800a5;

    @IdRes
    public static final int search_plate = 0x7f0800a6;

    @IdRes
    public static final int search_src_text = 0x7f0800a7;

    @IdRes
    public static final int search_voice_btn = 0x7f0800a8;

    @IdRes
    public static final int select_dialog_listview = 0x7f0800a9;

    @IdRes
    public static final int selected = 0x7f0800aa;

    @IdRes
    public static final int shortcut = 0x7f0800ab;

    @IdRes
    public static final int showCustom = 0x7f0800ac;

    @IdRes
    public static final int showHome = 0x7f0800ad;

    @IdRes
    public static final int showTitle = 0x7f0800ae;

    @IdRes
    public static final int smallLabel = 0x7f0800af;

    @IdRes
    public static final int snackbar_action = 0x7f0800b0;

    @IdRes
    public static final int snackbar_text = 0x7f0800b1;

    @IdRes
    public static final int snap = 0x7f0800b2;

    @IdRes
    public static final int spacer = 0x7f0800b3;

    @IdRes
    public static final int split_action_bar = 0x7f0800b4;

    @IdRes
    public static final int src_atop = 0x7f0800b5;

    @IdRes
    public static final int src_in = 0x7f0800b6;

    @IdRes
    public static final int src_over = 0x7f0800b7;

    @IdRes
    public static final int start = 0x7f0800b8;

    @IdRes
    public static final int stop = 0x7f0800b9;

    @IdRes
    public static final int stretch = 0x7f0800ba;

    @IdRes
    public static final int submenuarrow = 0x7f0800bb;

    @IdRes
    public static final int submit_area = 0x7f0800bc;

    @IdRes
    public static final int tabMode = 0x7f0800bd;

    @IdRes
    public static final int tag_transition_group = 0x7f0800be;

    @IdRes
    public static final int text = 0x7f0800bf;

    @IdRes
    public static final int text2 = 0x7f0800c0;

    @IdRes
    public static final int textSpacerNoButtons = 0x7f0800c1;

    @IdRes
    public static final int textSpacerNoTitle = 0x7f0800c2;

    @IdRes
    public static final int textViewInfo = 0x7f0800c3;

    @IdRes
    public static final int text_input_password_toggle = 0x7f0800c4;

    @IdRes
    public static final int textinput_counter = 0x7f0800c5;

    @IdRes
    public static final int textinput_error = 0x7f0800c6;

    @IdRes
    public static final int textinput_helper_text = 0x7f0800c7;

    @IdRes
    public static final int time = 0x7f0800c8;

    @IdRes
    public static final int title = 0x7f0800c9;

    @IdRes
    public static final int titleDividerNoCustom = 0x7f0800ca;

    @IdRes
    public static final int title_template = 0x7f0800cb;

    @IdRes
    public static final int top = 0x7f0800cc;

    @IdRes
    public static final int topPanel = 0x7f0800cd;

    @IdRes
    public static final int touch_outside = 0x7f0800ce;

    @IdRes
    public static final int transition_current_scene = 0x7f0800cf;

    @IdRes
    public static final int transition_layout_save = 0x7f0800d0;

    @IdRes
    public static final int transition_position = 0x7f0800d1;

    @IdRes
    public static final int transition_scene_layoutid_cache = 0x7f0800d2;

    @IdRes
    public static final int transition_transform = 0x7f0800d3;

    @IdRes
    public static final int tv = 0x7f0800d4;

    @IdRes
    public static final int tv1 = 0x7f0800d5;

    @IdRes
    public static final int tv2 = 0x7f0800d6;

    @IdRes
    public static final int tv3 = 0x7f0800d7;

    @IdRes
    public static final int uniform = 0x7f0800d8;

    @IdRes
    public static final int unlabeled = 0x7f0800d9;

    @IdRes
    public static final int up = 0x7f0800da;

    @IdRes
    public static final int useLogo = 0x7f0800db;

    @IdRes
    public static final int view_offset_helper = 0x7f0800dc;

    @IdRes
    public static final int visible = 0x7f0800dd;

    @IdRes
    public static final int withText = 0x7f0800de;

    @IdRes
    public static final int wrap_content = 0x7f0800df;
  }

  public static final class integer {
    @IntegerRes
    public static final int abc_config_activityDefaultDur = 0x7f090000;

    @IntegerRes
    public static final int abc_config_activityShortDur = 0x7f090001;

    @IntegerRes
    public static final int app_bar_elevation_anim_duration = 0x7f090002;

    @IntegerRes
    public static final int bottom_sheet_slide_duration = 0x7f090003;

    @IntegerRes
    public static final int cancel_button_image_alpha = 0x7f090004;

    @IntegerRes
    public static final int config_tooltipAnimTime = 0x7f090005;

    @IntegerRes
    public static final int design_snackbar_text_max_lines = 0x7f090006;

    @IntegerRes
    public static final int hide_password_duration = 0x7f090007;

    @IntegerRes
    public static final int mtrl_btn_anim_delay_ms = 0x7f090008;

    @IntegerRes
    public static final int mtrl_btn_anim_duration_ms = 0x7f090009;

    @IntegerRes
    public static final int mtrl_chip_anim_duration = 0x7f09000a;

    @IntegerRes
    public static final int show_password_duration = 0x7f09000b;

    @IntegerRes
    public static final int status_bar_notification_info_maxnum = 0x7f09000c;
  }

  public static final class string {
    @StringRes
    public static final int abc_action_bar_home_description = 0x7f0e0000;

    @StringRes
    public static final int abc_action_bar_up_description = 0x7f0e0001;

    @StringRes
    public static final int abc_action_menu_overflow_description = 0x7f0e0002;

    @StringRes
    public static final int abc_action_mode_done = 0x7f0e0003;

    @StringRes
    public static final int abc_activity_chooser_view_see_all = 0x7f0e0004;

    @StringRes
    public static final int abc_activitychooserview_choose_application = 0x7f0e0005;

    @StringRes
    public static final int abc_capital_off = 0x7f0e0006;

    @StringRes
    public static final int abc_capital_on = 0x7f0e0007;

    @StringRes
    public static final int abc_font_family_body_1_material = 0x7f0e0008;

    @StringRes
    public static final int abc_font_family_body_2_material = 0x7f0e0009;

    @StringRes
    public static final int abc_font_family_button_material = 0x7f0e000a;

    @StringRes
    public static final int abc_font_family_caption_material = 0x7f0e000b;

    @StringRes
    public static final int abc_font_family_display_1_material = 0x7f0e000c;

    @StringRes
    public static final int abc_font_family_display_2_material = 0x7f0e000d;

    @StringRes
    public static final int abc_font_family_display_3_material = 0x7f0e000e;

    @StringRes
    public static final int abc_font_family_display_4_material = 0x7f0e000f;

    @StringRes
    public static final int abc_font_family_headline_material = 0x7f0e0010;

    @StringRes
    public static final int abc_font_family_menu_material = 0x7f0e0011;

    @StringRes
    public static final int abc_font_family_subhead_material = 0x7f0e0012;

    @StringRes
    public static final int abc_font_family_title_material = 0x7f0e0013;

    @StringRes
    public static final int abc_search_hint = 0x7f0e0014;

    @StringRes
    public static final int abc_searchview_description_clear = 0x7f0e0015;

    @StringRes
    public static final int abc_searchview_description_query = 0x7f0e0016;

    @StringRes
    public static final int abc_searchview_description_search = 0x7f0e0017;

    @StringRes
    public static final int abc_searchview_description_submit = 0x7f0e0018;

    @StringRes
    public static final int abc_searchview_description_voice = 0x7f0e0019;

    @StringRes
    public static final int abc_shareactionprovider_share_with = 0x7f0e001a;

    @StringRes
    public static final int abc_shareactionprovider_share_with_application = 0x7f0e001b;

    @StringRes
    public static final int abc_toolbar_collapse_description = 0x7f0e001c;

    @StringRes
    public static final int action_settings = 0x7f0e001d;

    @StringRes
    public static final int app_name = 0x7f0e001e;

    @StringRes
    public static final int appbar_scrolling_view_behavior = 0x7f0e001f;

    @StringRes
    public static final int bottom_sheet_behavior = 0x7f0e0020;

    @StringRes
    public static final int character_counter_pattern = 0x7f0e0021;

    @StringRes
    public static final int dfu_action_abort = 0x7f0e0022;

    @StringRes
    public static final int dfu_confirmation_dialog_title = 0x7f0e0023;

    @StringRes
    public static final int dfu_feature_title = 0x7f0e0024;

    @StringRes
    public static final int dfu_file_size_label = 0x7f0e0025;

    @StringRes
    public static final int dfu_settings_dfu = 0x7f0e0026;

    @StringRes
    public static final int dfu_status_abored = 0x7f0e0027;

    @StringRes
    public static final int dfu_status_aborted = 0x7f0e0028;

    @StringRes
    public static final int dfu_status_aborted_msg = 0x7f0e0029;

    @StringRes
    public static final int dfu_status_completed = 0x7f0e002a;

    @StringRes
    public static final int dfu_status_completed_msg = 0x7f0e002b;

    @StringRes
    public static final int dfu_status_connecting = 0x7f0e002c;

    @StringRes
    public static final int dfu_status_connecting_msg = 0x7f0e002d;

    @StringRes
    public static final int dfu_status_disconnecting = 0x7f0e002e;

    @StringRes
    public static final int dfu_status_disconnecting_msg = 0x7f0e002f;

    @StringRes
    public static final int dfu_status_error = 0x7f0e0030;

    @StringRes
    public static final int dfu_status_error_msg = 0x7f0e0031;

    @StringRes
    public static final int dfu_status_initializing = 0x7f0e0032;

    @StringRes
    public static final int dfu_status_starting = 0x7f0e0033;

    @StringRes
    public static final int dfu_status_starting_msg = 0x7f0e0034;

    @StringRes
    public static final int dfu_status_switching_to_dfu = 0x7f0e0035;

    @StringRes
    public static final int dfu_status_switching_to_dfu_msg = 0x7f0e0036;

    @StringRes
    public static final int dfu_status_uploading = 0x7f0e0037;

    @StringRes
    public static final int dfu_status_uploading_components_msg = 0x7f0e0038;

    @StringRes
    public static final int dfu_status_uploading_msg = 0x7f0e0039;

    @StringRes
    public static final int dfu_status_uploading_part = 0x7f0e003a;

    @StringRes
    public static final int dfu_status_validating = 0x7f0e003b;

    @StringRes
    public static final int dfu_status_validating_msg = 0x7f0e003c;

    @StringRes
    public static final int dfu_unknown_name = 0x7f0e003d;

    @StringRes
    public static final int dfu_upload_dialog_cancel_message = 0x7f0e003e;

    @StringRes
    public static final int dfu_uploading_label = 0x7f0e003f;

    @StringRes
    public static final int dfu_uploading_percentage_label = 0x7f0e0040;

    @StringRes
    public static final int drawer_close = 0x7f0e0041;

    @StringRes
    public static final int drawer_open = 0x7f0e0042;

    @StringRes
    public static final int drawer_plugin_mcp = 0x7f0e0043;

    @StringRes
    public static final int fab_transformation_scrim_behavior = 0x7f0e0044;

    @StringRes
    public static final int fab_transformation_sheet_behavior = 0x7f0e0045;

    @StringRes
    public static final int hide_bottom_view_on_scroll_behavior = 0x7f0e0046;

    @StringRes
    public static final int mtrl_chip_close_icon_content_description = 0x7f0e0047;

    @StringRes
    public static final int no_apps = 0x7f0e0048;

    @StringRes
    public static final int no_ble = 0x7f0e0049;

    @StringRes
    public static final int not_available = 0x7f0e004a;

    @StringRes
    public static final int password_toggle_content_description = 0x7f0e004b;

    @StringRes
    public static final int path_password_eye = 0x7f0e004c;

    @StringRes
    public static final int path_password_eye_mask_strike_through = 0x7f0e004d;

    @StringRes
    public static final int path_password_eye_mask_visible = 0x7f0e004e;

    @StringRes
    public static final int path_password_strike_through = 0x7f0e004f;

    @StringRes
    public static final int scanner_action_cancel = 0x7f0e0050;

    @StringRes
    public static final int scanner_action_scan = 0x7f0e0051;

    @StringRes
    public static final int scanner_empty = 0x7f0e0052;

    @StringRes
    public static final int scanner_subtitle__not_bonded = 0x7f0e0053;

    @StringRes
    public static final int scanner_subtitle_bonded = 0x7f0e0054;

    @StringRes
    public static final int scanner_title = 0x7f0e0055;

    @StringRes
    public static final int search_menu_title = 0x7f0e0056;

    @StringRes
    public static final int status_bar_notification_info_overflow = 0x7f0e0057;
  }
}
