package com.philocare.philohub.activity;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.inuker.bluetooth.library.Code;
import com.inuker.bluetooth.library.Constants;
import com.inuker.bluetooth.library.model.BleGattProfile;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.search.response.SearchResponse;
import com.inuker.bluetooth.library.utils.BluetoothUtils;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;
import com.philocare.philohub.DeviceCompare;
import com.philocare.philohub.R;
import com.philocare.philohub.adapter.BleScanViewAdapter;
import com.philocare.philohub.adapter.CustomLogAdapter;
import com.philocare.philohub.adapter.DividerItemDecoration;
import com.philocare.philohub.adapter.OnRecycleViewClickCallback;
import com.veepoo.protocol.VPOperateManager;
import com.veepoo.protocol.listener.base.IABleConnectStatusListener;
import com.veepoo.protocol.listener.base.IABluetoothStateListener;
import com.veepoo.protocol.listener.base.IBleWriteResponse;
import com.veepoo.protocol.listener.base.IConnectResponse;
import com.veepoo.protocol.listener.base.INotifyResponse;
import com.veepoo.protocol.listener.data.IBPDetectDataListener;
import com.veepoo.protocol.listener.data.ICustomSettingDataListener;
import com.veepoo.protocol.listener.data.IDeviceFuctionDataListener;
import com.veepoo.protocol.listener.data.IHeartDataListener;
import com.veepoo.protocol.listener.data.IPwdDataListener;
import com.veepoo.protocol.listener.data.ISleepDataListener;
import com.veepoo.protocol.listener.data.ISocialMsgDataListener;
import com.veepoo.protocol.model.datas.BpData;
import com.veepoo.protocol.model.datas.FunctionDeviceSupportData;
import com.veepoo.protocol.model.datas.FunctionSocailMsgData;
import com.veepoo.protocol.model.datas.HeartData;
import com.veepoo.protocol.model.datas.PwdData;
import com.veepoo.protocol.model.datas.SleepData;
import com.veepoo.protocol.model.datas.SleepPrecisionData;
import com.veepoo.protocol.model.enums.EBPDetectModel;
import com.veepoo.protocol.model.enums.EFunctionStatus;
import com.veepoo.protocol.model.settings.CustomSettingData;
import com.veepoo.protocol.util.VPLogger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.veepoo.protocol.model.enums.EFunctionStatus.SUPPORT;

public class MainActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener, OnRecycleViewClickCallback {

    /**
     * O status gravado de volta
     */
    class WriteResponse implements IBleWriteResponse {

        @Override
        public void onResponse(int code) {
            Logger.t(TAG).i("write cmd status:" + code);

        }
    }

    TextView tv1, tv2, tv3;
    private final static String TAG = MainActivity.class.getSimpleName();
    private final static String YOUR_APPLICATION = "timaimee";
    Context mContext = MainActivity.this;
    private final int REQUEST_CODE = 1;
    List<SearchResult> mListData = new ArrayList<>();
    List<String> mListAddress = new ArrayList<>();
    //SwipeRefreshLayout mSwipeRefreshLayout;
    BleScanViewAdapter bleConnectAdatpter;

    private BluetoothManager mBManager;
    private BluetoothAdapter mBAdapter;
    private BluetoothLeScanner mBScanner;
    final static int MY_PERMISSIONS_REQUEST_BLUETOOTH = 0x55;
    //RecyclerView mRecyclerView;
    ListView mListView;
    TextView mTitleTextView;
    TextView mInfoTextView;
    VPOperateManager mVpoperateManager;
    private boolean mIsOadModel;

    Message msg;

    /**
     * Verificação de senha para obter as seguintes informações
     */
    int watchDataDay = 3;
    int contactMsgLength = 0;
    int allMsgLenght = 4;
    private int deviceNumber = -1;
    private String deviceVersion;
    private String deviceTestVersion;
    boolean isOadModel = false;
    boolean isNewSportCalc = false;
    boolean isInPttModel = false;
    boolean isSleepPrecision = false;

    // Variáveis para o timer
    private int time = 20;
    //private Timer timer;

    private Timer main_timer;

    MainActivity.WriteResponse writeResponse = new MainActivity.WriteResponse();

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String s = msg.obj.toString();
//            Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();

            switch (msg.what) {
                case 1:
                    tv1.setText(s + "\n");
                    break;
                case 2:
                    tv2.setText(s + "\n");
                    break;
                case 3:
                    tv3.setText(s + "\n");
                    break;
            }
        }
    };

    /*
    Timer principal do hub
     */
    public void startMainTimer() {
        main_timer = new Timer();
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // tenta conectar com a pulseira
                        connectDevice("E5:E0:3B:42:83:32", "");
                    }
                });
            }
        };
        main_timer.scheduleAtFixedRate(timerTask, 0, 60000);
    }

    /*
    Timer para executar as medições
     */
    /*public void startTimer() {
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        //textView.setText(String.format(Locale.getDefault(), "%d", time));
                        startHRMeasuring();
                        //startBPMeasuring();
                        if (time > 0)
                            time -= 1;
                        else {
                            //textView.setText(R.string.hello_world);
                            //btnStart.setChecked(false);
                        }
                    }
                });
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 60000);
    }*/



    private void sendMsg(String message, int what) {
        msg = Message.obtain();
        msg.what = what;
        msg.obj = message;
        mHandler.sendMessage(msg);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);

        initLog();
        Logger.t(TAG).i("onSearchStarted");
        mVpoperateManager = mVpoperateManager.getMangerInstance(mContext.getApplicationContext());
        VPLogger.setDebug(true);

        // prepara a tela de scaneamento inicial
        initRecyleView();

        // verifica as permissões do usuário android
        checkPermission();

        // registra um listener para o status de bluetooth pois ele sempre te de estar ligado para
        // que o aplicativo funcione
        registerBluetoothStateListener();

        // inicia o processo principal
        startMainTimer();
    }

    /*
    Medição de pressão sanguinea
     */
    public void startBPMeasuring() {
        tv1.setText("Espere 50s...");
        VPOperateManager.getMangerInstance(mContext).startDetectBP(writeResponse, new IBPDetectDataListener() {
            @Override
            public void onDataChange(BpData bpData) {
                String message = "Pressão :" + bpData.toString();
                tv2.setText(message);
                Logger.t(TAG).i(message);
                //sendMsg(message, 1);
                if (bpData.toString().contains("progress=100")) {
                    tv1.setText(Oprate.BP_DETECT_STOP);
                    VPOperateManager.getMangerInstance(mContext).stopDetectBP(writeResponse, EBPDetectModel.DETECT_MODEL_PUBLIC);
                }
            }
        }, EBPDetectModel.DETECT_MODEL_PUBLIC);
    }

    /*
    Começa a medir batimentos
     */
    public void startHRMeasuring() {
        tv1.setText("Medição HR");
        VPOperateManager.getMangerInstance(mContext).startDetectHeart(writeResponse, new IHeartDataListener() {

            @Override
            public void onDataChange(HeartData heart) {
                String message = "Pulso: " + heart.toString();

                SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                String format = s.format(new Date());

                tv2.setText(format + " " + message);

                Logger.t(TAG).i(message);
                //sendMsg(message, 1);
                // ############################ Leu batimentos uma vez, pára a leitura #######################
                if (heart.toString().contains("NORMAL")) {
                    //Logger.t(TAG).i("HEART_DETECT_STOP");
                    VPOperateManager.getMangerInstance(mContext).stopDetectHeart(writeResponse);
                    mVpoperateManager.disconnectWatch(writeResponse);
                }
            }
        });
    }

    public void readSleepData() {
        VPOperateManager.getMangerInstance(mContext).readSleepData(writeResponse, new ISleepDataListener() {
                    @Override
                    public void onSleepDataChange(SleepData sleepData) {
                        String message = "";
                        if (sleepData instanceof SleepPrecisionData && isSleepPrecision) {
                            SleepPrecisionData sleepPrecisionData = (SleepPrecisionData) sleepData;
                            message = "Accurate sleep data-back:" + sleepPrecisionData.toString();
                        } else {
                            message = "Normal sleep data-return:" + sleepData.toString();
                        }
                        Logger.t(TAG).i(message);
                        sendMsg(message, 1);
                    }

                    @Override
                    public void onSleepProgress(float progress) {

                        String message = "Sleep data-read progress:" + "progress=" + progress;
                        Logger.t(TAG).i(message);
                    }

                    @Override
                    public void onSleepProgressDetail(String day, int packagenumber) {
                        String message = "Sleep data-reading progress:" + "day=" + day + ",packagenumber=" + packagenumber;
                        Logger.t(TAG).i(message);
                    }

                    @Override
                    public void onReadSleepComplete() {
                        String message = "Sleep data-end of reading";
                        Logger.t(TAG).i(message);
                    }
                }, watchDataDay
        );
    }

    public void readSleepFrom () {
        int beforeYesterday = 2;
        VPOperateManager.getMangerInstance(mContext).readSleepDataFromDay(writeResponse, new ISleepDataListener() {
                    @Override
                    public void onSleepDataChange(SleepData sleepData) {
                        String message = "Sleep data-return:" + sleepData.toString();
                        Logger.t(TAG).i(message);
                        sendMsg(message, 1);
                    }

                    @Override
                    public void onSleepProgress(float progress) {
                        String message = "Sleep data-reading progress:" + "progress=" + progress;
                        Logger.t(TAG).i(message);
                    }

                    @Override
                    public void onSleepProgressDetail(String day, int packagenumber) {
                        String message = "Sleep data-reading progress:" + "day=" + day + ",packagenumber=" + packagenumber;
                        Logger.t(TAG).i(message);
                    }

                    @Override
                    public void onReadSleepComplete() {
                        String message = "Sleep data-end of reading";
                        Logger.t(TAG).i(message);
                    }
                }
                , beforeYesterday, watchDataDay);
    }

    public void readSleepSingleDay () {
        int yesterday = 1;
        VPOperateManager.getMangerInstance(mContext).readSleepDataSingleDay(writeResponse, new ISleepDataListener() {
            @Override
            public void onSleepDataChange(SleepData sleepData) {
                String message = "Sleep data-return:" + sleepData.toString();
                Logger.t(TAG).i(message);
                sendMsg(message, 1);
            }

            @Override
            public void onSleepProgress(float progress) {
                String message = "Sleep data-reading progress:" + "progress=" + progress;
                Logger.t(TAG).i(message);
            }

            @Override
            public void onSleepProgressDetail(String day, int packagenumber) {
                String message = "Sleep data-reading progress:" + "day=" + day + ",packagenumber=" + packagenumber;
                Logger.t(TAG).i(message);
            }

            @Override
            public void onReadSleepComplete() {
                String message = "Sleep data-end of reading";
                Logger.t(TAG).i(message);
            }
        }, yesterday, watchDataDay);
    }

    public void authDevice() {
        boolean is24Hourmodel = false;
        tv1.setText("Autenticado!");
        VPOperateManager.getMangerInstance(mContext).confirmDevicePwd(writeResponse, new IPwdDataListener() {
            @Override
            public void onPwdDataChange(PwdData pwdData) {
                String message = "PwdData:\n" + pwdData.toString();
                Logger.t(TAG).i(message);
                //sendMsg(message, 1);

                deviceNumber = pwdData.getDeviceNumber();
                deviceVersion = pwdData.getDeviceVersion();
                deviceTestVersion = pwdData.getDeviceTestVersion();
            }
        }, new IDeviceFuctionDataListener() {
            @Override
            public void onFunctionSupportDataChange(FunctionDeviceSupportData functionSupport) {
                String message = "FunctionDeviceSupportData:\n" + functionSupport.toString();
                Logger.t(TAG).i(message);
                //sendMsg(message, 2);
                EFunctionStatus newCalcSport = functionSupport.getNewCalcSport();
                if (newCalcSport != null && newCalcSport.equals(SUPPORT)) {
                    isNewSportCalc = true;
                } else {
                    isNewSportCalc = false;
                }
                watchDataDay = functionSupport.getWathcDay();
                contactMsgLength = functionSupport.getContactMsgLength();
                allMsgLenght = functionSupport.getAllMsgLength();
                isSleepPrecision = functionSupport.getPrecisionSleep() == SUPPORT;
            }
        }, new ISocialMsgDataListener() {
            @Override
            public void onSocialMsgSupportDataChange(FunctionSocailMsgData socailMsgData) {
                String message = "FunctionSocailMsgData:\n" + socailMsgData.toString();
                Logger.t(TAG).i(message);
                //sendMsg(message, 3);
            }
        }, new ICustomSettingDataListener() {
            @Override
            public void OnSettingDataChange(CustomSettingData customSettingData) {
                String message = "FunctionCustomSettingData:\n" + customSettingData.toString();
                Logger.t(TAG).i(message);
                //sendMsg(message, 4);
            }
        }, "0000", is24Hourmodel); // <== Aqui está a senha de acesso a F21
        // ################### Autenticou, pede leitura dos batimentos
        // inicia as leituras aqui

        //if (isChecked) {
        //    time = 20;
            //startHRMeasuring();
            readSleepSingleDay();
            //startTimer();
        //} else {
        //    timer.cancel();
        //    timer.purge();
        //    textView.setText(R.string.hello_world);
        //}
        //startHRMeasuring();

        //startBPMeasuring();

    }

    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.scan:
                scanDevice();
                break;
        }
    }

    private void initRecyleView() {
        //mSwipeRefreshLayout = (SwipeRefreshLayout) super.findViewById(R.id.mian_swipeRefreshLayout);
        //mRecyclerView = (RecyclerView) super.findViewById(R.id.main_recylerlist);
        mTitleTextView = (TextView) super.findViewById(R.id.main_title);

        //mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        bleConnectAdatpter = new BleScanViewAdapter(this, mListData);
        //mRecyclerView.setAdapter(bleConnectAdatpter);
        //mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL_LIST));
        bleConnectAdatpter.setBleItemOnclick(this);
        //mSwipeRefreshLayout.setOnRefreshListener(this);

        mTitleTextView.setText("Philo Hub " + getAppVersion(mContext));

        mInfoTextView = (TextView) super.findViewById(R.id.textViewInfo);

    }


    private void checkPermission() {
        Logger.t(TAG).i("Build.VERSION.SDK_INT =" + Build.VERSION.SDK_INT);
        if (Build.VERSION.SDK_INT <= 22) {
            initBLE();
            return;
        }

        int permissionCheck = ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Logger.t(TAG).i("checkPermission,PERMISSION_GRANTED");
            initBLE();
        } else if (permissionCheck == PackageManager.PERMISSION_DENIED) {
            requestPermission();
            Logger.t(TAG).i("checkPermission,PERMISSION_DENIED");
        }
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                Logger.t(TAG).i("requestPermission,shouldShowRequestPermissionRationale");

            } else {
                Logger.t(TAG).i("requestPermission,shouldShowRequestPermissionRationale else");
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_BLUETOOTH);
            }
        } else {
            Logger.t(TAG).i("requestPermission,shouldShowRequestPermissionRationale hehe");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_BLUETOOTH: {
                Logger.t(TAG).i("onRequestPermissionsResult,MY_PERMISSIONS_REQUEST_BLUETOOTH ");
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initBLE();
                } else {
                }
                return;
            }
        }
    }

    private void initLog() {
        Logger.init(YOUR_APPLICATION)
                .methodCount(0)
                .methodOffset(0)
                .hideThreadInfo()
                .logLevel(LogLevel.FULL)
                .logAdapter(new CustomLogAdapter());
    }

    // Ativa o scaneamento de pulseira pela biblioteca da F21
    private boolean scanDevice() {
        if (!mListAddress.isEmpty()) {
            mListAddress.clear();
        }
        if (!mListData.isEmpty()) {
            mListData.clear();
            bleConnectAdatpter.notifyDataSetChanged();
        }

        if (!BluetoothUtils.isBluetoothEnabled()) {
            Toast.makeText(mContext, "\n" +
                    "Bluetooth is not turned on", Toast.LENGTH_SHORT).show();
            return true;
        }
        mVpoperateManager.startScanDevice(mSearchResponse);
        return false;
    }


    private void connectDevice(final String mac, final String deviceName) {

        mVpoperateManager.registerConnectStatusListener(mac, mBleConnectStatusListener);

        mVpoperateManager.connectDevice(mac, deviceName, new IConnectResponse() {

            @Override
            public void connectState(int code, BleGattProfile profile, boolean isoadModel) {
                if (code == Code.REQUEST_SUCCESS) {
                    //Bluetooth and device connection status
                    Logger.t(TAG).i("connection succeeded");
                    Logger.t(TAG).i("Whether it is firmware upgrade mode=" + isoadModel);
                    mIsOadModel = isoadModel;
                } else {
                    Logger.t(TAG).i("Connection failed");
                    // não consegiu conectar, inicia o scan para tentar encontar
                    scanDevice();
                }
            }
        }, new INotifyResponse() {
            @Override
            public void notifyState(int state) {
                if (state == Code.REQUEST_SUCCESS) {
                    //Status de conexão do dispositivo e Bluetooth
                    Logger.t(TAG).i("Successful monitoring-can perform other operations");

                    Intent intent = new Intent(mContext, OperaterActivity.class);
                    intent.putExtra("isoadmodel", mIsOadModel);
                    intent.putExtra("deviceaddress", mac);
                    mInfoTextView.setText("Conectado a " + mac);
                    // ############## Conexão com sucesso ###############################
                    // Autenticar!!!
                    authDevice();
                    //startActivity(intent);
                } else {
                    Logger.t(TAG).i("Listen failed, reconnect");
                    // não consegiu conectar, inicia o scan para tentar encontar
                    tv1.setText("Pulseira não encontrada, procurando...");
                    scanDevice();
                }

            }
        });
    }


    /**
     * Bluetooth on or off
     */
    private void registerBluetoothStateListener() {
        mVpoperateManager.registerBluetoothStateListener(mBluetoothStateListener);
    }


    /**
     * Monitor the callback status of system Bluetooth on and off
     */
    private final IABleConnectStatusListener mBleConnectStatusListener = new IABleConnectStatusListener() {

        @Override
        public void onConnectStatusChanged(String mac, int status) {
            if (status == Constants.STATUS_CONNECTED) {
                Logger.t(TAG).i("STATUS_CONNECTED");
            } else if (status == Constants.STATUS_DISCONNECTED) {
                Logger.t(TAG).i("STATUS_DISCONNECTED");
                // pulseira não encontrada
                tv1.setText("Pulseira desconectada");
                //tv2.setText("");
            }
        }
    };

    /**
     * Monitor the callback status between Bluetooth and the device
     */
    private final IABluetoothStateListener mBluetoothStateListener = new IABluetoothStateListener() {
        @Override
        public void onBluetoothStateChanged(boolean openOrClosed) {
            Logger.t(TAG).i("open=" + openOrClosed);
        }
    };


    /**
     * Scanning callback
     */
    private final SearchResponse mSearchResponse = new SearchResponse() {
        @Override
        public void onSearchStarted() {
            Logger.t(TAG).i("onSearchStarted");
        }

        @Override
        public void onDeviceFounded(final SearchResult device) {
            Logger.t(TAG).i(String.format("device for %s-%s-%d", device.getName(), device.getAddress(), device.rssi));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!mListAddress.contains(device.getAddress())) {
                        mListData.add(device);
                        mListAddress.add(device.getAddress());
                        // verificar aqui se achoou a pulseira que queremos
                        // senão coneça a procurar de novo
                        if (device.getAddress() == "E5:E0:3B:42:83:32") {
                            connectDevice("E5:E0:3B:42:83:32", "");
                        } else {
                            scanDevice();
                        }
                    }
                    Collections.sort(mListData, new DeviceCompare());
                    bleConnectAdatpter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onSearchStopped() {
            refreshStop();
            Logger.t(TAG).i("onSearchStopped");
        }

        @Override
        public void onSearchCanceled() {
            refreshStop();
            Logger.t(TAG).i("onSearchCanceled");
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (BluetoothUtils.isBluetoothEnabled()) {
                scanDevice();
            } else {
                refreshStop();
            }
        }

    }

    // Refresh do swipe to refresh, ativa o scaneamento de pulseiras  próximas
    @Override
    public void onRefresh() {
        Logger.t(TAG).i("onRefresh");
        if (checkBLE()) {
            scanDevice();
        }
    }

    private void initBLE() {
        mBManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (null != mBManager) {
            mBAdapter = mBManager.getAdapter();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBScanner = mBAdapter.getBluetoothLeScanner();
        }
        checkBLE();

    }

    /**
     * Check if the Bluetooth device is turned on
     *
     * @return
     */
    private boolean checkBLE() {
        if (!BluetoothUtils.isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    /**
     * End refresh
     */
    void refreshStop() {
        Logger.t(TAG).i("refreshComlete");
        //if (mSwipeRefreshLayout.isRefreshing()) {
        //    mSwipeRefreshLayout.setRefreshing(false);
        //}
    }

    @Override
    public void OnRecycleViewClick(int position) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, "Connecting, please wait...", Toast.LENGTH_SHORT).show();
            }
        });
        SearchResult searchResult = mListData.get(position);
        connectDevice(searchResult.getAddress(), searchResult.getName());
    }

    public String getAppVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
