package com.philocare.philohub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sxr.sdk.ble.keepfit.client.R;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}